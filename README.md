# Year4 - Master project

The repo for my year 4 project

[Project site](https://projects.imaps.aber.ac.uk/projdescr.php?cym=0&pid=345)

Check out the [diary](https://gitlab.com/siaInSpace/mmp4/-/blob/master/diary/diary.pdf) for almost week by week entries of the tought process.

The final [report](https://gitlab.com/siaInSpace/mmp4/-/blob/master/report/main.pdf) is also available.

Finnaly the programs can also be found, of these the most important are [Main](https://gitlab.com/siaInSpace/mmp4/-/blob/master/program/main.py), [Crater](https://gitlab.com/siaInSpace/mmp4/-/blob/master/program/Crater_tools/Crater.py) and [Pyshtools_read](https://gitlab.com/siaInSpace/mmp4/-/blob/master/program/Crater_tools/pyshtools_read.py)
