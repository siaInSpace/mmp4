\section{Data extraction and investigative methods}
\subsection{Data extraction}
\label{sse:data-extract}
Following the literature review detailed in \cref{sec:lit}, it was decided to compound a basin catalogue from relevant sources. These sources were: 
\textcite{j_oberst} contributing some proposed basins, \textcite{grail_complex_to_baisin} with a list mostly focused on transitional complex craters to larger 
multi ring basins, \textcite{GRAIL_1} with a list of 74 relevant entries, \textcite{cook_1} with some proposed and confirmed basins and finally 
\textcite{old_catalog} another catalogue referencing a number of other sources. Each of these either provided information on new basins or served as an 
additional reference point for more well known basins. However, it is worth noting that some of these sources reference each other in their entries, which was 
not accounted for when creating this catalogue resulting in some instances where the same original data was added twice with different sources. This was not 
deemed a problem as, which is discussed later, the script developed to handle the creation of this catalogue accounted for basins with multiple sources.

The first script (Crater.py) %TODO ref.
handles the creation of this catalogue, utilising three classes, the reader is directed to \Cref{flw:catalog} for a visual overview of the process. The main 
class of these is the \enquote{Crater} class, this class holds the name of the basin/crater and a list of references for the crater. This list of references 
uses the \enquote{Crater\_source} class, which holds information from a single entry of a source i.e the position and size of the crater. It also contains a 
variable for the third class \enquote{Reference} which is used as a shorthand for which source provided the entry. These classes contains methods to handle the 
creation of the catalogue such as creating entries, checking for duplicate entries, reading and writing entries from a CSV file and default handling of sorting 
alphabetically. It also handles the situation where multiple sources are found for the same basin, based on its name, by creating a new entry averaged from all 
the sources for the specific basin. Finally the creation of Crater objects and the other classes are done iteratively over each entry of all the sources found, 
detailed above, in the \enquote{get\_craters} function to return a dictionary of all the basins.

The second script (Pyshtools\_read.py) %TODO ref Pyshtools_read
serves as an interface from the catalogue of Crater objects and the pyshtools package\cite{pypyshtools}.
The pyshtools library was created to handle spherical harmonic models in python, it also contains several relevant datasets for Lunar topology and gravity 
disturbance measurements. Other datasets are also available such as magnetic potential, and similar datasets are available for other celestial bodies in our 
solar system. For more information the reader is referred to pyshtools website. %TODO ref pyshtools website (https://shtools.github.io/SHTOOLS/index.html)
The two main parts of this script are extracting the datasets from pyshtools and extracting data from the spherical harmonic model based on the information 
from a Crater object. How this information is used to extract data sorounding a given crater can be seen in \Cref{flw:crater_data}.
\input{method/flowcharts/flow_crater}

\input{method/flowcharts/flow_pysh_r}
When using the pyshtools package the process of extracting the \gls{dem} becomes trivial. However, the user still have to decide on the dataset to use or to 
provide a sperhical harmonic model manually.
For this project it was decided to use one of the datasets provided, the MoonTopo2600p dataset as its 
upper limit of order and degree of 2600 could 
potentially provide a very detailed \gls{dem}. For the Bouguer anomaly map additional calculation was needed as only datasets of gravity disturbance was given, 
it 
was decided to use the GRGM1200B data set for this as it provided relatively high detail for a gravity disturbance map. To convert from gravity disturbance to 
Bouguer anomaly \Cref{eq:pre_bouguer_anomaly} was used to find the Bouguer correction ($\delta g_B$) . Substituting in the values for the gravitational 
constant $G = 6.67\times 10^{-11}N\ m^2\ kg^{-2}$ which can be converted to mGal using $1mGal = 0.01m\ s^{-2}$ to get $6.67 \times 10^{-6}mGal\ m^2\ kg^{-1}$ 
and the lunar crustal density $\rho = 2560kg\ m^{-3}$ \cite{grail_complex_to_baisin} we get \Cref{eq:bouguer_anomaly}. This is then multiplied by the height 
values from MoonTopo2600p resulting a spherical harmonic model of the Bouguer correction. This represents the expected gravity disturbance as a result of the 
material, or 
lack of material between the reference height of the Moon and the measured height at a given location. In addition the free air gravity correction is also 
needed. This represents the change in gravity as a result of moving away from the reference height and its correlated reference gravity. The free air gravity 
alone does not account for the material above or below the reference height and thus the Bouguer correction is needed. Finally the Bouguer correction and free 
air correction is subtracted from the measured gravity to give the Bouguer anomaly, how much the gravity disturbance measurements differs from the approximated 
model.

\begin{align}
	\delta g_B &= 2\pi \rho G H
	\label{eq:pre_bouguer_anomaly}
\end{align}
\begin{align}
	\delta g_B &= 2\pi \times 2560 \times 6.67\times10^{-6} \times H \nonumber\\
	\delta g_B &= 107.35\mu Gal\ m^{-1} \times H
	\label{eq:bouguer_anomaly}
\end{align}

\subsection{Data investigation}
\label{sse:data_invest}
The following will give an overview over the different methods used to investigate basin data, both topologically and gravitationally. For a more detailed 
comparison of the methods the reader is refereed to \Cref{sec:discussion}. Additionally, for more detail into the methods the reader is welcome to examine the 
relevant code at 
%TODO ref gitlab
and companioning documentation at %TODO ref code docs
The methods used can be split into two main categories; those developed for use on topological data and those for Bouguer anomalies. It was initially hoped 
that 
methods could work for both data types, but the differing shapes and nature of the data types made this difficult.

\subsubsection*{\gls{dem} (\acrlong{dem})}
From \Cref{fig:dem-bailly} and \Cref{fig:dem-orientale} we can see example images of the data extracted using the scripts detailed above. The Bailly basin is 
one of the smallest basins, with a diameter of 300km, and from \Cref{fig:simple_complex_basin} is most similar to that of a protobasin. The Orientale basin on 
the other hand is on the larger side with a diameter of 934km, and exemplifies a peak ring or multi ring basin. Typical to both of these examples and other 
basins, not shown, is a significant higher ring structure surrounding the basin, the rim, and a significant lower area inside this rim, the basin floor. For 
the topological data the main purpose will be to locate the basin rim in order to calculate its diameter.

Using \Cref{alg:top-std} we can examine the generalised start method used by the topological methods. Here the circular nature of impact basins in order 
to assume that data at a specific radius out from the basin will be similar to that of data at another location at the same distance out from the basin. Thus 
iteratively rotating the data through the specified data and extracting horizontal lines of data from each rotation. The horizontal data extract is then given 
to the investigate method and the results are stored. Finally once the results from each rotation is stored the results are used to approximate the diameter of 
the basin, this process will be different for each of the methods, and lastly the resulting diameter is returned. This process results in a way where multiple 
readings can be compounded together in order to approximate all the values from a two dimensional image. Using this starting method allows us to reduce the 
complexity of the data and allows the usage of the investigate methods, while also keeping most of the data available.

\SetAlgoSkip{bigskip}
\begin{algorithm}[H]
\SetAlgoLined
% \SetAlgoSkip{medskip}
\For{Rotation degree 0$\rightarrow$x}{
	Rotate data\;
	Extract centre horizontal line of data\;
	Apply investigate method\;
	Store results\;
}
Find diameter from stored results\;
\KwRet{diameter}
\caption{Topology generalised start method}
\label{alg:top-std}
\end{algorithm}

The first method, Max, can be considered the simplest of the three, it finds the position of the tallest value in the given data and returns it. This 
point should 
represent one of the basin rims as the formation of the basin pushes the rims upwards. For this method, readings 
are taken from zero to 360 degrees rotation. Results of this method on the Bailly basin can be seen in \Cref{fig:max-bailly}. We can note the symmetry 
around 180 degrees as a result of finding the same data point twice, only mirrored to the other side of the basin. The readings seems to cluster horizontally 
around index 90 and 420. Using the mean of these readings we can find the centre (index 260) and we can approximate the radius using the standard deviation of 
all the values.
\begin{figure}[H]
	\includegraphics[width=\linewidth]{images/Bailyy_y_max_pos_rotated_2020-02-17}
	\caption{Max height method rotated across the Bailly basin}
	\label{fig:max-bailly}
	%h!\belowcaptionskip{0}
\end{figure}

For the second method, Fit, inspiration was drawn from the modelling of optical spectroscopy where the location and characteristics of peaks has to be 
determined. %TODO ref
As such, this method uses fitted curves by utilising the lmfit python library %TODO ref
which handles the creation of composite models as well as a least square fitting to the data. An extract of a run using this method can be seen in 
\Cref{fig:curve-bailly} where the red line denotes the curve best fitted to the data in blue. In addition, the black dotted line represents the initial fit 
made 
by examining the available data and making an educated guess. Several composite models were considered, the most promising being a compound model of two 
Gaussian functions, one for each rim, and a more complex model consisting of three step arctangent functions. It was decided to use the three step functions as 
this provided more flexibility and also proved to better fit to the basin floor. From the extract of the Bailly basin (\Cref{fig:curve-bailly}) we can see 
that the initial fit was able to find both the left and right rim of the basin, and after the fitting this significantly improved on the left peak and while 
the right does show a smaller peak, the model decided to instead fit to the basin rim and an average value on the right. This highlights some of the robustness 
of the compound model, it is able to give some relevant information even when the data does not conform to the standard basins shape.

It is worth noting the significance the initial fit has on the performance of this method. A good guess will not only help guide the fitting towards 
identifying features, it will also significantly reduce the run time of this method. The run time of this method is substantially larger than that of the Max 
and Peak and thus any help it can get to reduce this is welcome. 
\begin{figure}[H]
	\includegraphics[width=0.5\textwidth]{images/Bailly_Step_Fit_2020-02-10}
	\caption{Curve fit on a horizontal line extract for the Bailly basin}
	\label{fig:curve-bailly}
\end{figure}

The third method used a function from the scipy library %TODO ref
, signal.find\_peaks. This method, as the name suggests, attempts to find peaks in signal. In this a case the horizontal data extracts and scores them on their 
height, width and most importantly, prominence. This method did show promising initial results, but with some issues. While using the prominence of a peak as 
a 
determinant we can expect the rim peaks to be the largest and thus the most prominent. However, the noisiness of the data and some background data 
often results in inaccurate readings. This lead to the creation of a clean method in order to reduce the amount of peaks. Cleaning the results is done by 
selecting the rightmost and leftmost peaks as starting points then looping over the other peaks found and averaging them with the location of either the left or 
right peaks depending on which is closest. This continues until only two peaks are left and these are returned as the left and right rim peak allowing to 
approximate the diameter as the difference between them.


\subsubsection*{Bouguer anomaly}
Observing \Cref{fig:ba-bailly} and \Cref{fig:ba-orientale} we can see how the Bouguer anomaly data differs from that of the topological. Firstly, we can note 
that the 
lowered crater floor has been replaced by a central peak. Secondly, the inner ring structure and rim has been replaced by a lowered annulus surrounding the 
central peak. Finally we can note that the multi ring structure seen in the topological data does not appear with the same magnitude in the gravitational data. 
The 
result of this is a more generalised profile that hints at easier modelling. 

From looking at the Bouguer anomaly data and the performance of the methods used on the topological data, see \Cref{sec:discussion}, it was decided to move 
forward with two of the methods, Peak and Fit. The differing profile from topological to gravitational data resulted in a shift in objective of these methods.
Instead of mapping to the basin rim, they should attempt to identify characteristics of the central peak and possibly the lowered annulus region.

\input{method/flowcharts/flow_get_peak_data}
As a result of using a lower resolution dataset the noisiness of the data has increased, this has lead to two counteractive methods. The first is using a 
lowpass 
filter, similar to the approach of previous articles\cite{GRAIL_1, grail_complex_to_baisin}, this reduced some of the noise, but not all. As the data still 
contained a lot of noise, using the investigate methods on a single horizontal line proved difficult, so it was decided to adapt the generalised starting point 
for the method. Instead of using the methods on each horizontal extract, multiple horizontal extracts would be averaged to provide more robust data to the 
methods, this also helped reduce run time by reducing the times each method was run. A flowchart for this new starting method can be seen in 
\Cref{flw:get_peak_data}. 

This new method is similar to the one described in \textcite[section 2.2 and figure 2]{grail_complex_to_baisin}. However, where they chose to average radially 
across 360 degrees around the basin, this work averages over 180 degrees across the diameter of the basin. This was done in order to maintain as much of 
the original data as possible, where the left and right side of the diameter represent the radially averaged top and bottom half of the basin respectively. A 
drawing of this can be seen in \Cref{fig:rot-mean} where the centre line splits the data into two halves. The three dotted lines serves as examples of three 
line extracts, where the blue and red parts are averaged together from their respective halves. The blue half circle shows all the data points averaged to a 
single point on the left side of the diameter and the red half symmetrically for the right side.
\begin{figure}[H]
	\centering
	\includegraphics[width=0.45\textwidth]{images/180-median}
	\caption{Drawing showing the averaging method. red and blue dotted lines show examples of extracted data with the red and blue half circles showing one 
area 
of the averaged data. Red denotes the right side of the line and thus the bottom section and blue the lift side of the line and top section.}
	\label{fig:rot-mean}
\end{figure}

As the general profile has changed to a central peak with a lowered annulus, the methods have to be adapted. For the Fit method this means choosing a new 
composite model. Based on the model for topological data, using three arctangent step functions, the change comes in reducing to only two step functions to 
model the central peak and a quadratic function to account for background data. This leaves out the lowered annulus from the model which helps simplify it and 
reduce run time, the negative annulus will have some impact on the quadratic model result in an often concave background model. Using this method we are able 
to extract the magnitude of the central peaks as can compare this with some previous findings.

Secondly, the Peak method has been adapted to locate the central peak rather than the basin rims. This was accomplished by using the prominence attribute from 
the find\_peaks function from scipy and relies on the the central peak to dominate its surrounding area which results in a significantly distinguishable 
prominence factor. This is often the case and thus should serve as a good factor when examining basin structure.
In addition, with the new method 
for averaging the data to a single horizontal line, the prominence attribute can also be used to locate the position of the lowered annulus region on either 
side of the peak. From this both the magnitude of the peak and the radius of the annulus can be found and used to approximate the width of a basin. 

Finally the method used by \textcite{GRAIL_1}, subsequently referenced as the annulus method, has also been implemented to serve as an assurance that the data 
extracted from the dataset is correct while also possibly providing an additional source of confirmation on their data. While a description of this method can 
be found in their work in the \enquote{materials and method} section, look for Bouguer anomaly contrast, on page 8, a description will be given here.

Using \Cref{fig:ba-mask} as a guide we can note the blue area in the shape of an annulus with an inner and outer diameter that of 50\% and 100\% of the basin 
diameter respectively. This area is used to calculate a base value for the Bouguer anomaly. Secondly, the area of the inner circle in green is used to find 
the peak intensity of the Bouguer anomaly using a diameter of 20\% of the basins diameter. All the data points in each of these areas are averaged and then 
the difference between the annulus and inner circle is used as the Bouguer contrast.

\subsection*{Experimental methods}
Two other methods were also considered, but in the end abandoned in favour for methods which provided better results, or simpler implementation. The first of 
these were using two dimensional models and fitting them to the data, similar to the fit method above. This would reduce the amount of 
preprocessing required by directly using the data available, hopefully giving more accurate results. Moving to a two dimensional model add complexity to the 
model not only in terms of parameters, but also in the data used for the least-square fitting. With this method we are able to remove some of the reliance the 
other methods have on prior knowledge, especially with reference to the centre of the basin. After some experimentation a capped Gaussian model was chosen 
together with a constant offset value, this allows the model to move vertically adapting to the background data while also fitting to the peak and plateau of 
some basins. 
\begin{figure}[H]
\centering
\includegraphics[width=\linewidth]{images/mask_vertical}
\caption{Masked area for finding the difference between the outer annulus (in blue) and the inner circle (in green) of the Orientale basin. Note the darker 
shading on the right hand image is done intentionally in order to highlight the blue and green colouring.}
\label{fig:ba-mask}
\end{figure}

Secondly the use of edge detection and the possibility of detecting the spherical structure of a basin rim using image processing such as Hough circle 
detection was attempted. This was implemented by subtracting a Gaussian blurred version of the data form the original data in order to highlight edges followed 
by thresholding to remove the weaker edges. After creating the edge image Hough circle detection was applied in order to detect the rim of the 
basin.

