#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Integrates my Crater_map classes and attempts to get information from crater locations."""

__date__ = "2021-03-01"
__author__ = "Sindre Aalhus"

import matplotlib.pyplot as plt
import pyshtools as pysh
import numpy as np
import scipy.ndimage.filters 
import scipy.signal
import lmfit
import scipy.optimize as opt
import csv
import tqdm
import uncertainties
#Add path to my classes
import sys
package_path = '/home/saa/Documents/mmp4'
if (not package_path in sys.path):
    sys.path.append(package_path)
import program.Crater_tools.Crater as Crater
import program.Crater_tools.pyshtools_read as pysh_r

#Get how many km per degree longitude which is dependent on the latitude.
km_deg = lambda lat : pysh.constants.Moon.r.value / 1000 * 2 * np.pi / 360 * np.cos(np.radians(lat))            

#Peak functions
def scipy_peaks(data, *args, **kwargs):
    """
    Find peaks in data using scipy.signal.find_peaks().

    Parameters
    ----------
    data : numpy array
        A signal with peaks
    *args : 
        arguments to pass to scipy.signal.find_peaks
    **kwars :
        named arguments to pass to scipy.signal.find_peaks

    Returns
    -------
    tuple
        Indices of array and list of information for peaks

    """
    return scipy.signal.find_peaks(data, *args, **kwargs)

def max_pos(data):
    """
    Find the position of the heighest value in the data.

    Parameters
    ----------
    data : numpy array
        A signal with peaks

    Returns
    -------
    int
        Index of highest value in data

    """
    return (np.where(data == np.amax(data)))[0][0]

def fitted_peaks(data, plot=False):
    """
    Attempt to fit a functions to the data using lmfit.

    Parameters
    ----------
    data : numpy array
        A signal with peaks
    plot : bool, optional
        Wheter or not to display a plot of fit The default is False.

    Returns
    -------
    left_peak : int
        position of the left peak
    right_peak : int
        position of the right peak

    """
    x=np.array(range(len(data)))
    #Initilize two rectangle models
    rec1 = lmfit.models.RectangleModel(prefix='rec1_', form='atan')
    rec2 = lmfit.models.RectangleModel(prefix='rec2_', form='atan')
    
    #Create parameters
    #center is the position of the max value in the data, amplitude is the max value in the data
    params = rec1.make_params(center1 = (np.where(data == np.amax(data)))[0][0],
                          center2 = (np.where(data == np.amax(data)))[0][0]+50,
                          amplitude = np.max(data))
    #Rec2 is shifted to the right.
    params += rec2.make_params(amplitude = np.max(data), 
       #step center is equal distant, but opposite x center as rec center
       center1 = (len(data)/2 - params['rec1_center1'].value) + len(data)/2, 
       center2 = (len(data)/2 - params['rec1_center1'].value) + len(data)/2 + 50)
        
    model = rec1 + rec2
        
    #Evaluate and fit
    init = model.eval(params, x=x)
    out = model.fit(data, params,x=x)
    
    
    if(plot):
        #Plot fit
        plt.figure()
        plt.plot(x, data, 'b', label='Data')
        plt.plot(x, init, 'k--', label='Initial fit')
        plt.plot(x, out.best_fit, 'r-', label='Best fit')
        plt.legend(loc='best')
        plt.savefig('Bailly_step_fit.png', dpi=200)
            
        #Plot components
        plt.figure()
        comps = out.eval_components(x=x)
        plt.plot(x, data, 'b', label='Data')
        plt.plot(x, comps['rec1_'], 'g--', label='Rec_1')
        plt.plot(x, comps['rec2_'], 'r--', label='Rec_2')
        plt.legend(loc='best')
        plt.show()
    
    #Get peak values
    best_params = out.best_values
    left_peak, right_peak = 0, 0
    #Check which peak is on which side.
    if (best_params['rec1_center2'] < best_params['rec2_center1']):
        left_peak = best_params['rec1_center2']
        right_peak = best_params['rec2_center1']
    else:
        left_peak = best_params['rec2_center1']
        right_peak = best_params['rec1_center2'] 
    return (left_peak, right_peak)


def from_max(crater_analyser, plot=False):
    """
    Get center and radius using the max_pos function.

    Parameters
    ----------
    crater_analyser : Crater_analyser
        The crater analyser object to use

    Returns
    -------
    float
        center_x
    float
        center_y
    float
        diameter

    """
    #Iterate over multiple angles
    fits = []
    for i in range(0, 360, 1):
        fits.append(crater_analyser.find_peaks(max_pos, i))
    fits = np.array(fits)
    #Plot data
    if (plot):
        plt.figure()
        plt.scatter(np.array(range(0, 360, 1)), fits, label='highest value')
        plt.plot([0, 360], [fits.mean(), fits.mean()], 'g--', 
                 label='mean (center)')
        plt.plot([0, 360], [fits.mean()+fits.std(), fits.mean()+fits.std()],
                 'r--', label='std (radius)')
        plt.plot([0, 360], [fits.mean()-fits.std(), fits.mean()-fits.std()], 
                 'r--')
        plt.xlabel("Rotation (degrees)" )
        plt.ylabel("Position of highest value")
        plt.legend(loc='upper right', bbox_to_anchor=(1.35, 1))
        #plt.savefig('rotate_max.png', dpi=200)
        plt.show()
        
    #Get center from mean and diameter from standard deviation * 2.
    center_x = fits.mean()
    center_y = crater_analyser.crater_data.shape[1]/2
    diameter = fits.std()*2
    return ((center_x, center_y), diameter)

def from_fitted(crater_analyser):
    """
    Fits to each rotated horizontal line, then avrages results and approximates center and diameter.

    Parameters
    ----------
    crater_analyser : Crater_analyser
        Crater analyser holding the data to use.

    Returns
    -------
    float
        center x.
    float
        center y.
    float
        diameter.

    """
    fits = []
    for i in range(0, 180, 1):
        fits.append(crater_analyser.find_peaks(fitted_peaks, i))
    fits = np.array(fits)
    peaks = np.median(fits, 0)
    #print(fits)
    return ((peaks.mean(), crater_analyser.crater_data.shape[1]/2), 
            peaks.std()*2)
    
def from_scipy(crater_analyser, distance=40, prominence=1):
    """
    Get center using the scipy.signal.find_peaks method.

    Parameters
    ----------
    crater_analyser : Crater_analyser
        Crater analyser object holding the data to use.
    distance : float, optional
        The minimum distance between peaks. The default is 40.
    prominence : float, optional
        The minimum prominence of peaks. The default is 1.

    Returns
    -------
    float
        center x.
    float
        center y.
    float
        diameter.
    """
    peaks = []
    for i in range(0, 360, 1):
        tmp = crater_analyser.find_peaks(scipy_peaks, i, distance=distance, 
                                         prominence=prominence)[0]
        peaks.append(tmp) 
    simplified_peaks = np.array(simplify_peaks(peaks))
    
    return ((simplified_peaks.mean(), crater_analyser.crater_data.shape[1]/2),
            simplified_peaks.std()*2)


def simplify_peak(peak):
    """
    Reduce the number of peaks detected to two. Is called recursivly.

    Parameters
    ----------
    peak : list
        The position of the peaks found.

    Raises
    ------
    ValueError
        If the peaks sent are less than two.

    Returns
    -------
    list
        The simplified list of peaks.

    """
    if (len(peak) < 2):
        raise ValueError("Peak must be of length 2 or greater (len(peak):{})".format(len(peak)))
    if (len(peak) == 2):
        #Exit condition
        return peak
    
    #Initilize left and rightmost peaks.
    left = peak[0]
    right = peak[-1]
    
    #Get extra peaks and find distance to left and right peak.
    extra_peak = peak[1]
    distance_left = extra_peak - left
    distance_right = right - extra_peak
    
    #Find which is closest and adjust the corresponding peak.
    if (distance_left < distance_right):
        left = (left + extra_peak) / 2
        #print("left")
    elif (distance_right < distance_left):
        right = (right + extra_peak) / 2
        #print("right")
   
    
    peak[0] = left
    peak[-1] = right
    #Remove peak at pos 1
    peak = peak[np.arange(len(peak))!=1]
    
    #Recursive call
    return simplify_peak(peak)
   

def simplify_peaks(peaks):
    """
    Simplify interivly through a list of a list of peak positions.

    Parameters
    ----------
    peaks : list
        list of list of peak positions.

    Returns
    -------
    nicer_peaks : list
        Simplified list.

    """
    nicer_peaks = []
    for p in peaks:
        length = len(p)
        if (length < 2):
            #Remove entries with one or zero peaks
            break
        nicer_peaks.append(simplify_peak(p))
    return nicer_peaks



def compare_methods(c_map, use_grav=False):
    """
    Compare methods to get center and diameter postions.

    Parameters
    ----------
    c_map : Crater_map
        Crater map object to use.
    use_grav : bool, optional
        If false use topology, otherwise use Gravity. The default is False.

    Returns
    -------
    errs_grav : list
        A list of how the error from each method, for each crater.

    """
    errs_grav = {}
    offset = 0
    for key in tqdm.tqdm(sorted(c_map.craters.keys())[offset:]):
        try:
            crater_data, scale, px_km = c_map.get_crater_map(key, 512,
                                                             use_grav)
        except:
            continue
        
        #Initilise objects
        c_map_handler = pysh_r.Crater_map_handler(crater_data, scale, px_km)
        c_analyse = pysh_r.Crater_analyser(c_map_handler.get_crater_map())
        #crater = c_map.craters[key]
        
        #Make sure we have data, otherwise skip to next crater
        if (type(c_analyse.crater_data) == type(None)):
            continue
            
        # #Get crater data
        # crater_data, scale, px_km = c_map.get_crater_map(key, 512, use_grav)
        # c_map_handler = pysh_r.Crater_map_handler(crater_data, scale, px_km)
        # c_analyse = pysh_r.Crater_analyser(c_map_handler.get_crater_map())
         
        
        # #Make sure we have data, otherwise skip to next.
        # if (type(c_analyse.crater_data) == type(None)):
        #     continue
        
        crater = c_map.craters[key]

        
        #Run the methods
        center_max, diameter_max = from_max(c_analyse)
        center_fitted, diameter_fitted = from_fitted(c_analyse)
        center_peak, diameter_peak = from_scipy(c_analyse)
    
        #Get diameter of crater
        d = 0
        for s in crater.sources:
            d = s.diameter
            if d != 0:
                break
        if (d == 0):
            break
        #Convert diameter from methods to km
        d_max = diameter_max/scale/px_km
        d_fitted  = diameter_fitted/scale/px_km
        d_peak = diameter_peak/scale/px_km        
    
    
        #Caclulate and append errors to list
        err = []
        
        err.append((center_max, 1-(d_max)))
        err.append((center_fitted, 1-(d_fitted)))
        err.append((center_peak, 1-(d_peak)))
        err = np.array(err)
        errs_grav[key] =  err
        #print("{}: Done".format(key))
   
    return errs_grav


def mean_grav_peaks():
    """
    Get the mean of multiple horizontal lines from rotating a crater, then fitting a curve to this data.
    
    Returns
    -------
    err : dict
        A dictonary holding the diameter and peaks found for each crater.
    """
    use_grav = True
    c_map = pysh_r.Crater_map(create_top=True, create_grav=use_grav)  
    offset = 0
    err = {}
    for key in (sorted(c_map.craters.keys())[offset:]):
        #Get data
        try:
            crater_data, scale, px_km = c_map.get_crater_map(key, 512,
                                                             use_grav)
        except:
            continue
        
        #Initilise objects
        c_map_handler = pysh_r.Crater_map_handler(crater_data, scale, px_km)
        c_analyse = pysh_r.Crater_analyser(c_map_handler.get_crater_map())
        #crater = c_map.craters[key]
        
        #Make sure we have data, otherwise skip to next crater
        if (type(c_analyse.crater_data) == type(None)):
            continue
        
        #Rotate crater and get the data for the center line
        d = []
        for i in range(0, 180, 1):
            d.append(c_analyse.find_peaks(np.array, i))
        d = np.array(d)
        #Calulate meadian of each point of center line for list of lines (axis=0).
        d = np.median(d, axis=0)
        #Get peaks of median
        pks = scipy.signal.find_peaks(d, prominence=100)
        #center_peak, diameter_peak = from_scipy(c_analyse) 
        #Save data to dict
        err[key] = (d, pks)
        print("{}: Done".format(key))
    return err


def twoD_Gaussian(xdata_tuple, amplitude, xo, yo, sigma_x, sigma_y, theta,
                  offset):
    """Two dimensional gausian function."""
    (x, y) = xdata_tuple                                                        
    xo = float(xo)                                                              
    yo = float(yo)                                                              
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)   
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)    
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)   
    g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo)         
                        + c*((y-yo)**2))) 
    #Return method flattened to one dimension.                                  
    return g.ravel()


def fit_2d_gauss(key='Orientale', use_grav=True, shape=512):
    """
    Attempt to fit two dimensional gaussian to data using scipy optimize.
    
    Note: uses flatting so might be eqivalent to using a one dimensional gaussian.

    Parameters
    ----------
    key : str, optional
        The key for the crater to use. The default is 'Orientale'.
    use_grav : bool, optional
        If flase will use topology, otherwise use gravity data. The default is True.
    shape : int, optional
        The shape to resize the data to. The default is 512.

    Returns
    -------
    None.
    """
    #Create x, y grid
    x = np.linspace(0, shape, shape)
    y = np.linspace(0, shape, shape)
    x, y = np.meshgrid(x, y)

    c_map = pysh_r.Crater_map(create_top=True, create_grav=use_grav)   
    data_2d, scale, px_km = c_map.get_crater_map(key, shape, use_grav)
    max_x , max_y = np.where(data_2d == np.max(data_2d))
    #max_x = [255]
    #max_y = [255]
    
    #Make a guess of the gaussian shape based on max positions and min, max values.
    initial_guess = (np.abs(data_2d.min()-data_2d.max()), max_x[0], max_y[0],
                     80, 80, 0, data_2d.min())
    
    #Get best fit parameters.
    popt, pcov = opt.curve_fit(twoD_Gaussian, (x, y), data_2d.ravel(), 
                               p0=initial_guess)
    
    #Calculate the gaussiand from the fitted parameters
    data_fitted = twoD_Gaussian((x, y), *popt).reshape(shape, shape)
    
    #Plot figure.
    plt.figure()
    plt.imshow(data_2d, cmap='jet')
    plt.contour(data_fitted, 8, colors='w')
    plt.figure()
    plt.plot(data_2d[:, int(shape/2)], label='Data')
    plt.plot(data_fitted[:, int(shape/2)], label='Fit')
    plt.legend()
    plt.ylabel("Bouguer anomaly (mGal)")
    
    #plt.figure()
    #plt.plot(data_2d.flatten())
    #plt.plot(data_fitted.flatten())  
    #return (data_2d, data_fitted, popt)


def plot_fit(fit, x=None, title=None, scale=None, px_km=None, lattitude=None, longitude=None, save_path=None):
    """
    Plot a fit from lmfit.
    
    Shows two subplots, one with data, fit and inital guess, 
    the other with data and the components of the fit.

    Parameters
    ----------
    fit : lmfit.ModelResult
        The reusult from lmfit.fit.
    x : list, optional
        The list to use for x values, if none is given will be generated from len(fit.data). The default is None.
    title : str, optional
        The title of the plot. The default is None.
    scale : float, optional
        The scale of the data used to calculate km distance from center. The default is None.
    px_km : float, optional
        The px per km used to calculate km distance from center. The default is None.
    lattitude : float, optional
        The lattitude of the crater center, used to calculate degrees from center. The default is None.
    longitude : float, optional
        The longitudde of the crater center, used to give degrees from center. The default is None.
    save_path : str, optional
        The path to save the plot to, if none is given plot will be shown directly. The default is None.

    Returns
    -------
    None.

    """
    plot_title = ""

    try:
        plot_title += title
    except TypeError:
        #No title given
        pass
    xlabel = ""
    if (not x):
        x = np.arange(len(fit.data))
        
    # if (scale and px_km):
    try:
        side_len = len(fit.data)/2/scale/px_km #km
        x = np.linspace(-side_len, side_len, len(fit.data)) 
        xlabel = "distance from center (km)"
    except (NameError, TypeError) :
        #scale or px_km or both not given
        pass
    
    try:
        side_len = len(fit.data)/2/scale/px_km #km
        km_deg_lat = km_deg(lattitude)
        plot_title += "\n({:.2f}N)".format(lattitude)

        left_lon = longitude - side_len/km_deg_lat
        right_lon = longitude + side_len/km_deg_lat
        x = np.linspace(left_lon, right_lon, len(fit.data))#  % 360
        xlabel = "longitude (deg)"
    except TypeError:
        #lat or lon or both not given
        pass
    
    
    fig, (ax1, ax2) = plt.subplots(1, 2, sharey=False, figsize=(16, 8)) #figsize=(14.2, 8)
   
    ax1.plot(x, fit.data, 'b', label='Data')
    ax1.plot(x, fit.init_fit, 'k--', label='Initial fit')
    ax1.plot(x, fit.best_fit, 'r-', label='Best fit')
    ax1.legend(loc=1)    
    ax1.title.set_text('Fit')    
    
    #Plot components
    
    ax2.plot(x, fit.data, 'b', label='Data')
    comps = fit.eval_components()
    for k in comps:
        ax2.plot(x, comps[k], '--', label=k)
    ax2.legend(loc=1)
    ax2.title.set_text('Components')
    

    fig.add_subplot(111, frame_on=False)
    plt.tick_params(labelcolor="none", bottom=False, left=False)
    plt.ylabel('Bouguer anomaly (mGal)', labelpad=25)
    plt.xlabel(xlabel)
    plt.title(plot_title)
    
    fig.tight_layout()
    
    try:
        plt.savefig(save_path, dpi=150, bbox_inches='tight')
    except AttributeError:
        #No path given, show plot instead
        plt.show()
    

def fitted_peaks2(data, plot=False, plot_title=None, scale=1, px_km=1,
                  lat=None, lon=None):
    """
    Second method to fit peaks using lmfit, uses a different compund model.

    Parameters
    ----------
    data : numpy.ndarray
        The data to fit.
    plot : bool, optional
        Wheter or not to plot the fit. The default is False.
    plot_title : str, optional
        The title to give the plot. The default is None.
    scale : float, optional
        The scale (px per px) to convert plot to distance in km from center. The default is 1.
    px_km : float, optional
        The px per km used to convert plot to distance in km from center. The default is 1.
    lat : float, optional
        The latitude (deg) of the center of the data / crater, used to convert plot to degrees. The default is 0.
    lon : float, optional
        The lattitude (deg) of the center of the data / crater, used to convert plot to degrees. The default is 0.

    Returns
    -------
    out : lmfit.model.ModelResult
        The result from the fit of the compund model.

    """
    #Quadratic model is versitile enough to handle most of the background shapes of a crater.
    #Can be a dip, rise or linear.    
    background = lmfit.models.QuadraticModel(prefix='background')
    
    #The atan rectangle model fits well to most crater shapes.
    #Works well for data with a direct peak and data with a platou.
    #The artan form smooths the edges.
    shape = lmfit.models.RectangleModel(prefix='peak', form='atan')

    c = np.where(data == np.amax(data))[0][0]
    
          
    params = background.make_params(c=np.min(data))
    
    amp = np.abs(np.max(data)-np.min(data))
    params += shape.make_params(
        amplitude=amp, 
        center1=c-40, center2=c+40)
    
    # params['peakamplitude'].max = 1500
    model = background + shape
    
    x = np.arange(len(data))
    # init = model.eval(params, x=x)
    out = model.fit(data, params,x=x)
    # comps = out.eval_components(x=x)
    
    if (not plot):
        return out

    plot_fit(out, x, plot_title, scale, px_km, lat)
    return out


def fit_one(crater, c_map, use_grav=True, shape=512):
    """
    Simmilar to mean_grav_peaks, gets the mean of multiple horizontal lines from a rotated crater.

    Parameters
    ----------
    crater : Crater.Crater
        The crater to get data from.
    c_map : Crater.Crater_map
        The Crater map to get the crater data from.
    use_grav : bool, optional
        Wheter or not to use gravity data or topology data. The default is True.
    shape : int, optional
        The shape to resize the crater data to. The default is 512.

    Returns
    -------
    out : lmfit.model.ModelResult
        The output from the fit.
    s : float
        The scale used when resising the data.
    px_km : float
        The px per km used when extracting the data from c_map.
    d : numpy.ndarray
        The data surrounding the crater, extracted from c_map

    """
    try:
        d, s, px_km  = c_map.get_crater_map(crater, 512, use_grav)
    except:
        print("No crater")
        return None
    crater_analyser = pysh_r.Crater_analyser(d, s, px_km)
            
    if (type(crater_analyser.crater_data) == type(None)):
        print("No analyser")
        return None
    fits = []
    for i in range(0, 180, 1):
        fits.append(crater_analyser.find_peaks(np.array, i))
    fits = np.array(fits)
    # medians = np.median(fits, 0)
    means = np.mean(fits, 0)

    lon = crater.sources[0].lon
    lat = crater.sources[0].lat

    out = fitted_peaks2(means, False, crater.name, s, px_km, lat, lon)
    return (out, s, px_km, d)



def fit_all(c_map, use_grav=True, shape=512):
    """
    Call fit_one iterativley for all the craters in the catalog.

    Parameters
    ----------
    c_map : Crater.Crater_map
        The crater map which hold the sperhical harmonic model for data extraction.
    use_grav : bool, optional
        Wheter to use gravity or topology data. The default is True.
    shape : int, optional
        The size to resize the extracted crater data to. The default is 512.

    Returns
    -------
    t : list
        A list of all the outputs from fit_one.

    """
    t = {}
    for crater in tqdm.tqdm(sorted(c_map.craters.values(), 
                         key=lambda item: item.sources[0].diameter)):
        o = fit_one(crater, c_map)
        if (type(o) == type(None)):
            continue
        t[crater.name] = o
    return t


def scatter_x(axes, data, x):
    """
    Extract y data using x and scatters it to the axis given.

    Parameters
    ----------
    axes : pyplot.AxesSubplot
        The axis to plot to.
    data : numpy.ndarray
        The data to extract the y values from.
    x : numpy.ndarray
        The x locations to use.

    Returns
    -------
    None.

    """
    y = data[x]
    axes.scatter(x, y)


def sweep_moon(c_map, func, d=500, lat_range=range(-90, 90, 10), lon_range=range(0, 360, 10)):
    """
    Create a series of custom craters to test locations on the Moon for craters.
    
    Saves the crater plots and returns their data.
    Uses tqdm to show progress bar and estimate time left. 
    Note: This does not work well when useing the spyder console.

    Parameters
    ----------
    c_map : Crater.Crater_map
        The crater map with the sperhicla harmonic models to use.
    d : float, optional
        The diameter to use for the custom craters. The default is 500.

    Returns
    -------
    data : list
        A list which holds the fits from the custom craters.

    """
    data = []
    ref = Crater.Refrence("S. Aalhus", year=2021)
    for lat in tqdm.tqdm(lat_range, desc='lat'):
        for lon in  tqdm.tqdm(lon_range, desc='lon({})'.format(lat)):
            crater_name = "custom_{}N_{}E_{}km".format(lat, lon, d)
            custom_crater = Crater.Crater(crater_name)
            custom_crater.add_source(Crater.Crater_source(lon, lat, d, ref))
            result = func(crater=custom_crater, c_map=c_map)
            data.append((lon, lat, result))
            # fit, s, px_km, init_data = fit_one(custom_crater, c_map)
            # plot_fit(fit, scale=s, px_km=px_km, longitude=lon, lattitude=lat, 
            #          title=crater_name, save_path="moon_sweep/"+crater_name+".png")
            # data.append([fit.best_fit, fit.data, init_data])
            # print("Done {:03d}N, {:03d}E at {:05d}km".format(int(lat), int(lon), int(d)))
    return data
            

def bouguer_contrast(c_map, crater, shape=512):
    """
    Calculate the bouguer contrast for a crater.
    
    Uses the a method from Neumann et. al (2015) doi:10.1126/sciadv.1500852.


    Parameters
    ----------
    c_map : Crater.Crater_map
        The crater map with the sperhical harmonic models to use.
    crater : Crater.Crater
        The crater to investigate.
    shape : int, optional
        The shape to rehsape the crater data to. The default is 512.

    Raises
    ------
    TypeError
        If the given crater has no sources that give diameter information.

    Returns
    -------
    contrast : uncertainties.ufloat
        The bouguer contrast with std.
    """
    data, scale, px_km  = c_map.get_crater_map(crater, shape, True)
        
    #Get crater diameter from its sources.
    d = 0
    for s in crater.sources:
        d = s.diameter
        if d != 0:
            break
    if d == 0:
        print("No diameter")
        raise TypeError("Crater missing diameter")
        
    #For circular mask creation 
    #Inspired by https://stackoverflow.com/questions/49330080/numpy-2d-array-selecting-indices-in-a-circle
    x = np.arange(0, shape)
    y = np.arange(0, shape)
    cx = shape/2
    cy = shape/2
    
    annulus_r100 = d/2*scale*px_km
    annulus_r50 = annulus_r100/2
    center_r20 = d/2*scale*px_km/5
    
    #Crater circular masks
    mask_outer_annulus = (x[np.newaxis,:]-cx)**2 + (y[:,np.newaxis]-cy)**2 < annulus_r100**2
    mask_inner_annulus = np.invert((x[np.newaxis,:]-cx)**2 + (y[:,np.newaxis]-cy)**2 < annulus_r50**2)
    mask_annulus = mask_outer_annulus & mask_inner_annulus
    
    mask_center = (x[np.newaxis,:]-cx)**2 + (y[:,np.newaxis]-cy)**2 < center_r20**2
    
    #Get the data
    annulus_data = data[mask_annulus]
    center_data = data[mask_center]

    #Return the absolute diffrence from their means
    contrast = uncertainties.ufloat(center_data.mean() - annulus_data.mean(), center_data.std())
    
    return contrast


def all_bouguer_contrast(c_map, shape=512, path='bouguer_anomaly.csv'):
    """
    Calulate the bouguer contrast for all craters in c_map.craters.

    Parameters
    ----------
    c_map : Crater.Crater_map
        The crater map with the sperhical harmonic models and craters to use.
    shape : int, optional
        The shape to reshape the crater data to. The default is 512.
    path : str, optional
        The path to save results to. The default is 'bouguer_anomaly.csv'.

    Returns
    -------
    None.

    """
    rows = []
    for crater in tqdm.tqdm(sorted(c_map.craters.values(), key=lambda c : c.sources[0].diameter)):
        #Get crater diameter from its sources.
        d = 0
        for s in crater.sources:
            d = s.diameter
            if d != 0:
                break
        if d == 0:
            print("{}: No diameter".format(crater.name))
            continue
        
        contrast = bouguer_contrast(c_map, crater, shape)
        row = [crater.name, crater.sources[0].lat, crater.sources[0].lon, d, contrast]
        rows.append(row)
        
    with open(path, 'w') as file:
        csv_writer = csv.writer(file)
        csv_writer.writerow(["Name", "Lat(N)", "Lon(E)", "Diameter(km)", "Bouguer contrast(mGal)"])
        csv_writer.writerows(rows)
        
    
if (__name__ == '__main__'):
    pass
    # use_grav=True
    # key='Bailly'
    # shape=512
    # c_map = pysh_r.Crater_map(create_top=True, create_grav=use_grav)
    # crater_bailly = c_map.craters['Bailly']
    # crater_orientale = c_map.craters['Orientale']
    # d, s, p = c_map.get_crater_map(crater_orientale, 512, False)
    # c_analyser = pysh_r.Crater_analyser(d, s, p)
    
    # ob, sb, pb, db = fit_one(crater_bailly, c_map)
    # oo, so, po, do = fit_one(crater_orientale, c_map)