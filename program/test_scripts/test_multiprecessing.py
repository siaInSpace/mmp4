#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 12 16:31:33 2021

@author: saa
"""

import multiprocessing as mp
import numpy as np
from tqdm import tqdm
import time

# np.random.RandomState(100)
# arr = np.random.randint(0, 10, size=[200000, 5])
# data = arr.tolist()
# # Step 1: Redefine, to accept `i`, the iteration number
# def howmany_within_range2(i, row, minimum, maximum):
#     """Returns how many numbers lie within `maximum` and `minimum` in a given `row`"""
#     count = 0
#     for n in row:
#         if minimum <= n <= maximum:
#             count = count + 1
#     return (i, count)


# # Step 2: Define callback function to collect the output in `results`
# def collect_result(result):
#     global results
#     results.append(result)
# # Parallelizing with Pool.starmap_async()


# pool = mp.Pool(mp.cpu_count())

# results = pool.starmap_async(howmany_within_range2, [(i, row, 4, 8) for i, row in enumerate(data)]).get()

# # With map, use `howmany_within_range_rowonly` instead
# # results = pool.map_async(howmany_within_range_rowonly, [row for row in data]).get()

# pool.close()
# print(results[:10])
# #> [3, 1, 4, 4, 4, 2, 1, 1, 3, 3]


def work(a, b):
    time.sleep(0.3)
    return a*b


if (__name__ == '__main__'):
    args = []
    for i in range(117):
        args.append((i, i))
    start = time.time()
    with mp.Pool() as p:
        runner = p.starmap_async(work, args)
        runner.wait()
    print(time.time() - start)
    
    
