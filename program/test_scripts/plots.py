#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  9 16:25:47 2021

@author: saa
"""

import csv
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import pandas as pd
import numpy as np
import cv2
import uncertainties
from mpl_toolkits.axes_grid1 import make_axes_locatable
import tqdm
#Add path to my classes
import sys
package_path = '/home/saa/Documents/mmp4'
if (not package_path in sys.path):
    sys.path.append(package_path)
import program.Crater_tools.Crater as Crater
import program.Crater_tools.pyshtools_read as pysh_r
import program.main as Crater_methods
import program.test_scripts.fitted_peak3_test as Peak3
import program.test_scripts.custom_2d_fit as c2d
plt.rcParams.update({'font.size': 20})

pyplot_figsize=(8, 7)

def grail1_bouguer_contrast_vis(c_map, crater, shape=512, cmap='gray', save_path=None):
    data, scale, px_km  = c_map.get_crater_map(crater, shape, True)
        
    #Get crater diameter from its sources.
    d = 0
    for s in crater.sources:
        d = s.diameter
        if d != 0:
            break
    if d == 0:
        print("No diameter")
        raise TypeError("Crater missing diameter")
        
    #For circular mask creation 
    #Inspired by https://stackoverflow.com/questions/49330080/numpy-2d-array-selecting-indices-in-a-circle
    x = np.arange(0, shape)
    y = np.arange(0, shape)
    cx = shape/2
    cy = shape/2
    
    annulus_r100 = d/2*scale*px_km
    annulus_r50 = annulus_r100/2
    center_r20 = d/2*scale*px_km/5
    
    #Crater circular masks
    mask_outer_annulus = (x[np.newaxis,:]-cx)**2 + (y[:,np.newaxis]-cy)**2 < annulus_r100**2
    mask_inner_annulus = np.invert((x[np.newaxis,:]-cx)**2 + (y[:,np.newaxis]-cy)**2 < annulus_r50**2)
    mask_annulus = mask_outer_annulus & mask_inner_annulus
    
    mask_center = (x[np.newaxis,:]-cx)**2 + (y[:,np.newaxis]-cy)**2 < center_r20**2
    
    center = mask_center*255
    b = np.ones_like(center, np.uint8) *0 
    g = center.astype(np.uint8)
    r = np.ones_like(center, np.uint8) *0
    center_bgr = cv2.merge((b, g, r))

    annulus = mask_annulus * 255
    r = annulus.astype(np.uint8)
    g = b.copy()
    annulus_bgr = cv2.merge((b, g, r))
    
    wimg = cv2.addWeighted(center_bgr, 1, annulus_bgr, 1, 0)
    # plt.imshow(wimg)

    data_bgr = cv2.cvtColor(pysh_r.Crater_map_handler.to_uint8(data), cv2.COLOR_GRAY2BGR)
    masked_data = cv2.addWeighted(wimg, 1, data_bgr, 0.5, 1)
    side_len = len(data)/2/scale/px_km 

    # plt.imshow(w2img, extent=[-side_len, side_len, -side_len, side_len])    
    # plt.title("Masked data")
    # plt.xlabel("Distnace from center (km)")
    # plt.ylabel("Distnace from center (km)")
    # masked_data = data.copy()
    # masked_data[mask_center] = np.median(data)
    # masked_data[mask_annulus] = np.median(data)
    
    
    fig, (ax1, ax2) = plt.subplots(2, 1, sharex=False, figsize=(6, 10.6))
    ax1.imshow(data, cmap=cmap, extent=[-side_len, side_len, -side_len, side_len])
    ax1.title.set_text('Data')    
    ax2.imshow(masked_data, cmap=cmap, extent=[-side_len, side_len, -side_len, side_len])    
    ax2.title.set_text('Masked data')    
    fig.add_subplot(111, frame_on=False)
    plt.tick_params(labelcolor="none", bottom=False, left=False)

    plt.title(crater.name + "\n")
    plt.xlabel("Distance from center (km)")
    plt.ylabel("Distance from center (km)")
    plt.tight_layout()
    
    try:
        plt.savefig(save_path, dpi=150, bbox_inches='tight')
    except AttributeError:
        plt.show()

def grail_1_bouguer_contrast(path='../../report/grail_1_bouguer_contrast.csv'):
    d = {}
    with open(path) as file:
        csv_reader = csv.reader(file, quoting=csv.QUOTE_NONNUMERIC)
        head = next(csv_reader)
        for r in csv_reader:
            d[r[0]] = (r[1], r[2])
    return d


def new_latex_table2(scipy2_data, fits, save_path, bouguer_contrast_path="new_bc.csv"):
    #Load grail1s data
    grail1_data = grail_1_bouguer_contrast()
    
    #Load my data
    with open(bouguer_contrast_path, 'r') as file:
        csv_reader = csv.reader(file)
        next(csv_reader)#Skip header
        rows = {}
        for row in csv_reader:
            #name, lat, lon, diameter, contrast
            rows[row[0]] = (float(row[1]), float(row[2]), float(row[3]), uncertainties.ufloat_fromstr(row[4]))
    
    head = "\hline {} & {:03.02f} & {:03.02f} & {:.0f}"    
    my_contrast = " & {:.3L}"
    grail1_contrast1 = " &{:.0f} $\\pm$ {:.0f}"
    grail1_contrast2 = " & {:.0f}"
    scipy_contrast = " & {:.3L}"
    fit_contrast = " & {:.0f}"
    tail = " \\\\\n"
    
    format1 = head + fit_contrast + scipy_contrast + my_contrast + grail1_contrast1 + tail
    format2 = head + fit_contrast + scipy_contrast + my_contrast + " & N/A" + tail
    format3 = head + fit_contrast + scipy_contrast + my_contrast + grail1_contrast2 + tail    
    
    
    with open(save_path, 'w') as file:
        for key in rows:
            lat, lon, diameter, bc_contrast = rows[key]
            scipy2_entry = scipy2_data[key]
            fit_data = Peak3.get_peak_val(fits[key][0])
            
            s = ""
            try:
                s = format1.format(key, lat, lon, diameter, fit_data, 
                           scipy2_entry, bc_contrast, grail1_data[key][0], grail1_data[key][1])
            except KeyError:
                #Grail1 does not have an entry with name
                s = format2.format(key, lat, lon, diameter, fit_data, 
                           scipy2_entry, bc_contrast)
            except ValueError:
                #Grail1 entry does not have std
                try:
                    s = format3.format(key, lat, lon, diameter, fit_data, 
                           scipy2_entry, bc_contrast, grail1_data[key][0])
                except ValueError:
                    #something weird has happend
                    s = format2.format(key, lat, lon, diameter, fit_data, 
                           scipy2_entry, bc_contrast)
            file.write(s)

def new_latex_table(scipy_signal, save_path, load_bouguer_contrast="new_bc.csv"):
    #Load grail1s data
    grail1_data = grail_1_bouguer_contrast()
    
    #Load my data
    with open(load_bouguer_contrast, 'r') as file:
        csv_reader = csv.reader(file)
        next(csv_reader)#Skip header
        rows = []
        for row in csv_reader:
            #name, lat, lon, diameter, contrast
            rows.append([row[0], float(row[1]), float(row[2]), float(row[3]), uncertainties.ufloat_fromstr(row[4])])
             
            
    name_lat_lon_d = "\hline {} & {:03.02f} & {:03.02f} & {:.0f}"
    my_contrast = " & {:.3L}"
    grail1_contrast1 = " &{:.0f} $\\pm$ {:.0f}"
    grail1_contrast2 = " & {:.0f}"
    scipy_contrast = " & {:.3L}"
    scipy_width = " & {:.3L}"
    tail = " \\\\\n"
    
    new_format1 = name_lat_lon_d + scipy_width + scipy_contrast + my_contrast + grail1_contrast1 + tail
    new_format2 = name_lat_lon_d + scipy_width + scipy_contrast + my_contrast + " & N/A" + tail
    new_format3 = name_lat_lon_d + scipy_width + scipy_contrast + my_contrast + grail1_contrast2 + tail
    
    with open(save_path, 'w') as file:    
        for row in rows:
            name = row[0]
            s = ""
            try:
                s = new_format1.format(name, row[1], row[2], row[3], scipy_signal[name][1], scipy_signal[name][0], row[4], grail1_data[name][0], grail1_data[name][1])
            except KeyError:
                #Grail1 does not have an entry with name
                s = new_format2.format(name, row[1], row[2], row[3], scipy_signal[name][1], scipy_signal[name][0], row[4])
            except ValueError:
                #Grail1 entry does not have std
                try:
                    s = new_format3.format(name, row[1], row[2], row[3], scipy_signal[name][1], scipy_signal[name][0], row[4], grail1_data[name][0])
                except ValueError:
                    #something weird has happend
                    s = new_format2.format(name, row[1], row[2], row[3], scipy_signal[name][1], scipy_signal[name][0], row[4])
            file.write(s)
     
    
def to_latex_table(load_path='bouguer_anomaly.csv', save_path='bouguer_contrast_table.tex'):
    d = grail_1_bouguer_contrast()
    
    with open(load_path, 'r') as file:
        csv_reader = csv.reader(file)
        next(csv_reader)#Skip header
        # print(head)
        rows = []
        for row in csv_reader:
            rows.append([row[0], float(row[1]), float(row[2]), float(row[3]), float(row[4]), float(row[5])])
    
    row_str = "\hline\t{} & {:03.02f} & {:03.02f} & {:03.02f} & {:03f} $\\pm$ {:03f} & N/A \\\\\n"
    
    row_str2 = "\hline\t{} & {:03.02f} & {:03.02f} & {:03.02f} & {:03f} $\\pm$ {:03f} & {:03f} $\\pm$ {:03f}\\\\\n"
    row_str3 = "\hline\t{} & {:03.02f} & {:03.02f} & {:03.02f} & {:03f} $\\pm$ {:03f} & {:03f}\\\\\n"
    
    with open(save_path, 'w') as file:    
        for row in rows:
            s = ""
            try:
                s = row_str2.format(row[0], row[1], row[2], row[3], row[4], row[5], d[row[0]][0], d[row[0]][1])
            except KeyError:
                s = row_str.format(row[0], row[1], row[2], row[3], row[4], row[5])
            except ValueError:
                try:
                    s = row_str3.format(row[0], row[1], row[2], row[3], row[4], row[5], d[row[0]][0])
                except ValueError:
                    s = row_str.format(row[0], row[1], row[2], row[3], row[4], row[5])
            file.write(s)
        

def plot_crater_image(c_map, key, shape=512, use_grav=False, cmap='gray', save_path=None):
    data, scale, px_km = c_map.get_crater_map(key, shape, use_grav)
    side_len = len(data)/2/scale/px_km 
    plt.figure(figsize=(8, 8))
    # plt.grid()
    ax = plt.gca()
    im = ax.imshow(data, cmap=cmap, extent=[-side_len, side_len, -side_len, side_len])

    plt.xlabel("distance from center (km)")
    plt.ylabel("distance from center (km)")
  
    try:
        plt.title(key.name)
    except AttributeError:
        plt.title(key)
    
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.2)
    colorbar_label = "Height (km)"
    if (use_grav):
        colorbar_label = "Bouguer anomaly (mGal)"
    plt.colorbar(im, cax=cax, label=colorbar_label)  
    
    plt.tight_layout()
    try:
        plt.savefig(save_path, dpi=150, bbox_inches='tight')
    except AttributeError:
        plt.show()
        
        
def plot_two_xy(c_map, craters, labels, colors=("C0", "C1"), use_grav=False, save_path=None):
    d0, s0, p0 = c_map.get_crater_map(craters[0], 512, use_grav)
    d1, s1, p1 = c_map.get_crater_map(craters[1], 512, use_grav)
    d0 = d0[255]
    d1 = d1[255]
    side_len0 = len(d0)/2/s0/p0 
    side_len1 = len(d1)/2/s1/p1 
    # print(side_len1)
    # d0, d1 = data
    l0, l1 = labels
    c0, c1 = colors
    fig=plt.figure(figsize=pyplot_figsize)
    ax=fig.add_subplot(111, label="1")
    ax2=fig.add_subplot(111, label="2", frame_on=False)
    
    ax.plot(np.linspace(-side_len0, side_len0, len(d0)), d0, color=c0, label=l0)
    ax.set_xlabel("Distance from center (km)", color=c0)
    ax.set_ylabel("Height (km)", color=c0)
    if (use_grav):
        ax.set_ylabel("Bouguer anomaly (mGal)", color=c0)    
    
    ax.tick_params(axis='x', colors=c0)
    ax.tick_params(axis='y', colors=c0)
    
    ax2.plot(np.linspace(-side_len1, side_len1, len(d1)), d1, color=c1, label=l1)
    ax2.xaxis.tick_top()
    ax2.yaxis.tick_right()
    ax2.set_xlabel('Distance from center (km)', color=c1) 
    ax2.set_ylabel('Height (km)', color=c1)       
    if (use_grav):
        ax2.set_ylabel("Bouguer anomaly (mGal)", color=c1) 
    ax2.xaxis.set_label_position('top') 
    ax2.yaxis.set_label_position('right') 
    ax2.tick_params(axis='x', colors=c1)
    ax2.tick_params(axis='y', colors=c1)
    line0 = mlines.Line2D([], [], color=c0, label=l0)
    line1 = mlines.Line2D([], [], color=c1, label=l1)
    plt.legend(handles=[line0, line1])
    plt.tight_layout()
    try:
        plt.savefig(save_path, dpi=150, bbox_inches='tight')
    except AttributeError:
        plt.show()


def plot_meaned(c_map, crater, use_grav=True, save_path=None):
    plt.figure(figsize=pyplot_figsize)
    d, s, p = c_map.get_crater_map(crater, 512, use_grav)
    c_analyser = pysh_r.Crater_analyser(d, s, p)
    md = []
    for i in range(0, 180, 1):
        md.append(c_analyser.find_peaks(np.array, i))
    md = np.array(md)
    side_len = len(d)/2/s/p
    plt.plot(np.linspace(-side_len, side_len, len(d[255])),  d[255], label='Sample data', color='b', linewidth=2)
    plt.plot(np.linspace(-side_len, side_len, len(d[255])), np.mean(md, axis=0), label='Averaged', color='red', linewidth=2)
    try:
        plt.title(crater.name)
    except AttributeError:
        plt.title(crater)
    plt.xlabel("Distance from center (km)")
    plt.ylabel("Height (km)")
    if (use_grav):
        plt.ylabel("Bouger anomaly (mGal)")
    plt.legend()
    plt.tight_layout()
    try:
        plt.savefig(save_path, dpi=150, bbox_inches='tight')
    except AttributeError:
        plt.show()
            

def plot_ba_dem(c_map, crater, save_path=None):
    dem_d, dem_s, dem_p = c_map.get_crater_map(crater, 512, False)
    ba_d, ba_s, ba_p = c_map.get_crater_map(crater, 512, True)
    dem_c_analyser = pysh_r.Crater_analyser(dem_d, dem_s, dem_p)    
    ba_c_analyser = pysh_r.Crater_analyser(ba_d, ba_s, ba_p)
    
    m_dem_d = []
    m_ba_d = []
    for i in range(0, 180, 1):
        m_dem_d.append(dem_c_analyser.find_peaks(np.array, i))
        m_ba_d.append(ba_c_analyser.find_peaks(np.array, i))
    m_dem_d = np.array(m_dem_d)
    m_ba_d =  np.array(m_ba_d)
    
    dem_data = np.mean(m_dem_d, axis=0)
    ba_data = np.mean(m_ba_d, axis=0)
    
    fig, ax1 = plt.subplots(figsize=pyplot_figsize)
    side_len = len(ba_data)/2/ba_s/ba_p
    x = np.linspace(-side_len, side_len, len(ba_data))
    ax1.plot(x, dem_data, color='b', linewidth=2)
    ax1.set_ylabel("Height (km)", color='b')
    ax1.tick_params(axis='y', colors='b')
    ax1.set_xlabel("Distance from center (km)")

    ax2 = ax1.twinx()
    ax2.plot(x, ba_data, color='r', linewidth=2)
    ax2.set_ylabel("Bouguer anomaly (mGal)", color='r')
    ax2.tick_params(axis='y', colors='r')
    try:
        plt.title(crater.name)
    except AttributeError:
        plt.title(crater)
    plt.tight_layout()
    try:
        plt.savefig(save_path, dpi=150, bbox_inches='tight')
    except AttributeError:
        plt.show()


def create_plot_figs(c_map, meaned=True, overlapped=True, crater_images=True):
    c_bailly = c_map.craters['Bailly']
    c_orientale = c_map.craters['Orientale']
    
    if (meaned):
        #dem meaned 
        plot_meaned(c_map, c_bailly, False, "dem_bailly_meaned.png")
        plot_meaned(c_map, c_orientale, False, "dem_orientale_meaned.png")
    
        
        #ba meaned
        plot_meaned(c_map, c_bailly, True, "ba_bailly_meaned.png")
        plot_meaned(c_map, c_orientale, True, "ba_orientale_meaned.png")
    
    if (overlapped):
        #dem, ba meaned overlapped
        plot_ba_dem(c_map, c_bailly, "ba_dem_bailly.png")
        plot_ba_dem(c_map, c_orientale, "ba_dem_orientale.png")
    
    if (crater_images):
        #crater images
        plot_crater_image(c_map, c_bailly, save_path="bailly_dem.png")
        plot_crater_image(c_map, c_bailly, save_path="bailly_ba.png", use_grav=True)
        
        plot_crater_image(c_map, c_orientale, save_path="orientale_dem.png")
        plot_crater_image(c_map, c_orientale, save_path="orientale_ba.png", use_grav=True)

def from_fit(data):
    fit, scale, px_km, inital_data = data
    return fit.best_values['peakamplitude']
    

def dem_compare_boxplot(c_map):
    topology_approx = Crater_methods.compare_methods(c_map, False)
    bouguer_scipy_signal2_approx = {}
    for key in tqdm.tqdm(sorted(c_map.craters.keys())):
        try:
            d, s, px_km  = c_map.get_crater_map(key, 512, False)
        except:
            print("No crater")
            return None
        crater_analyser = pysh_r.Crater_analyser(d, s, px_km)
                
        if (type(crater_analyser.crater_data) == type(None)):
            print("No analyser")
            return None
        fits = []
        for i in range(0, 180, 1):
            fits.append(crater_analyser.find_peaks(np.array, i))
        fits = np.array(fits)
        # medians = np.median(fits, 0)
        means = np.mean(fits, 0)
        bouguer_scipy_signal2_approx[key] = Peak3.scipy_signal2_get_height_and_width(means)
    return (topology_approx, bouguer_scipy_signal2_approx)

def dem_boxplot(compare_path, scipy2_path, c_map):
    df_compare = pd.read_csv(compare_path)
    df_scipy2 = pd.read_csv(scipy2_path)
    df_merged = pd.merge(df_compare, df_scipy2, on="name")
    
    #Remove old indexes
    df_merged.rename({"Unnamed: 0_y":"a"},axis="columns", inplace=True)
    df_merged.drop(["a"], axis=1, inplace=True)
    df_merged.rename({"Unnamed: 0_x":"a"},axis="columns", inplace=True)
    df_merged.drop(["a"], axis=1, inplace=True)
    
    #Reformat scipy2 data
    data_scipy2 = []
    for i in df_merged["width"]:
        a = uncertainties.ufloat_fromstr(i)
        data_scipy2.append(a.n)
    df_merged["d_scipy2"] = data_scipy2
    df_merged.drop(["width"], axis=1, inplace=True)
    
    diamters = pd.DataFrame(columns=["name", "diameter"])
    for key in c_map.craters:
        d = c_map.craters[key].sources[0].diameter
        diamters.loc[len(diamters)] = [key, d]
    
    df_merged = pd.merge(df_merged, diamters, on="name")
    
    df_errs = pd.DataFrame()
    for i in ["d_max", "d_fit", "d_peak", "d_scipy2"]:
        df_errs[i] = 1 -(df_merged[i] / df_merged["diameter"])
        
    df_errs.rename({"d_max":"max"},axis="columns", inplace=True)
    df_errs.rename({"d_fit":"fit"},axis="columns", inplace=True)
    df_errs.rename({"d_peak":"peak"},axis="columns", inplace=True)
    df_errs.rename({"d_scipy2":"bouguer peak"},axis="columns", inplace=True)

    return df_errs
    
def dem_boxplot_create_plot(df, save_path=None):
    f,(ax,ax2) = plt.subplots(2,1,sharex=True, facecolor='w', 
      gridspec_kw={'height_ratios': [8, 1]}, figsize=pyplot_figsize)

    df.boxplot(ax=ax)
    df.boxplot(ax=ax2)

    ax.set_ylim(-1.1, 1.)
    ax2.set_ylim(-3.2,-3)
    
    # hide the spines between ax and ax2
    ax.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    
    ax2.set_xticks([1, 2, 3, 4])
    #ax.set_xticks([])
    #f.subplots_adjust(hspace=0.0000001)
    
    d = .015 # how big to make the diagonal lines in axes coordinates
    kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
    
    #top right (x1, x2), (y1, y2)
    ax.plot((1-d,1+d), (0, 0), **kwargs)
    
    #top left
    # ax.plot((-d,+d),(-d,+d), **kwargs)
    ax.plot((-d,+d),(0, 0), **kwargs)
    
    kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
    # ax2.plot((-d,+d), (1-d,1+d), **kwargs)
    # ax2.plot((1-d,1+d), (1-d,1+d), **kwargs)
    ax2.plot((-d,+d), (1, 1), **kwargs)
    ax2.plot((1-d,1+d), (1, 1), **kwargs)
    
    ax2.set_xlabel('Fit method')
    ax.set_ylabel('Relative error')
    ax.set_title("Boxplot of relative diameter error using\ndifferent fit methods to find diameter")
    
    plt.tight_layout()
    try:
        plt.savefig(save_path, dpi=150, bbox_inches='tight')
    except AttributeError:
        plt.show()
    
def plot_2d_fit(c_map, crater, save_path=None):
    shape=512
    use_grav=True
    data, scale, px_km = c_map.get_crater_map(crater, shape, use_grav)
    fit = c2d.new_main(data)
    side_len = len(data)/2/scale/px_km 
    
    
    fig, (ax1, ax2) = plt.subplots(1, 2, sharex=False, figsize=(12, 5))

    #plt.figure(figsize=(8, 8))
    #ax = plt.gca()
    im = ax1.imshow(data, cmap='gray', extent=[-side_len, side_len, -side_len, side_len])
    # plt.xlabel("distance from center (km)")
    # plt.ylabel("distance from center (km)")
    
    im2 = ax2.imshow(fit.best_fit, cmap='gray', extent=[-side_len, side_len, -side_len, side_len])

    divider = make_axes_locatable(ax1)
    cax = divider.append_axes("right", size="5%", pad=0.2)
    colorbar_label = "Height (km)"
    if (use_grav):
        colorbar_label = "Bouguer anomaly (mGal)"
    plt.colorbar(im, cax=cax)#, label=colorbar_label)  
    
    divider = make_axes_locatable(ax2)
    cax2 = divider.append_axes("right", size="5%", pad=0.2)
    colorbar_label = "Height (km)"
    if (use_grav):
        colorbar_label = "Bouguer anomaly (mGal)"
    plt.colorbar(im2, cax=cax2)#, label=colorbar_label)  
    
    fig.add_subplot(111, frame_on=False)
    plt.tick_params(labelcolor="none", bottom=False, left=False)

    plt.title(crater.name + "\n(mGal)")
    plt.xlabel("Distance from center (km)")
    plt.ylabel("Distance from center (km)")
    plt.tight_layout()
  
    # try:
    #     plt.title(crater.name)
    # except AttributeError:
    #     plt.title(crater)
    
    
    plt.tight_layout()
    try:
        plt.savefig(save_path, dpi=150, bbox_inches='tight')
    except AttributeError:
        plt.show()
    
def plot_2d_fit_profile(c_map, crater, save_path=None):
    shape=512
    use_grav=True
    data, scale, px_km = c_map.get_crater_map(crater, shape, use_grav)
    fit = c2d.new_main(data)
    side_len = len(data)/2/scale/px_km
    
    fit_extract = fit.best_fit[255, :]
    data_extract = data[255, :]

    x = np.linspace(-side_len, side_len, len(data_extract))
    
    plt.figure(figsize=pyplot_figsize)
    plt.plot(x, data_extract, label="data")
    plt.plot(x, fit_extract, label="2d fit")
    plt.xlabel("Distance form centre (km)")
    plt.ylabel("Bouguer anomaly (mGal)")
    plt.title(crater.name)
    plt.legend()
    
    plt.tight_layout()
    try:
        plt.savefig(save_path, dpi=150, bbox_inches='tight')
    except AttributeError:
        plt.show()
    
    
def bailly_newton_crater(diameter=450):
    custom_crater = Crater.Crater("New Bailly-Newton")
    my_ref = Crater.Refrence("S. Aalhus", 2021)
    d = diameter
    lat = -77
    lon = -39
    source = Crater.Crater_source(lon, lat, d, my_ref)
    custom_crater.add_source(source)
    return custom_crater
    
def bailly_newton(c_map, diameter=450):
    custom_crater = bailly_newton_crater(diameter)  
    data_top = c_map.get_crater_map(custom_crater, 512, False)[0]
    fit, scale, px_km, data2d = Crater_methods.fit_one(custom_crater, c_map)
    
    cp, lp, rp = Peak3.scipy_signal2(fit.data, 20)
    return (custom_crater, (fit, scale, px_km, data2d, data_top), (cp, lp, rp))
    
if (__name__ == '__main__'):
    # c_map = pysh_r.Crater_map(create_top=True, create_grav=True)
    # d, p, s = c_map.get_crater_map('Orientale', 512, False)
    # db, p, s = c_map.get_crater_map('Bailly', 512, False)
    # load_path = '../new3.csv'
    # to_latex_table(load_path, 'new3.tex')
    pass
    

