#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 26 11:06:04 2021

@author: saa
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from brokenaxes import brokenaxes


errors_d_grav = np.loadtxt("errors_diameter_grav_2020-02-24.txt")
errors_d_top = np.loadtxt("errors_diameter_top_2020-02-24.txt")

errors_d_grav = pd.DataFrame(errors_d_grav, columns=['max', 'fit', 'peak'])
errors_d_top = pd.DataFrame(errors_d_top, columns=['max', 'fit', 'peak'])

def top_d_error_plot():
    f,(ax,ax2) = plt.subplots(2,1,sharex=True, facecolor='w', gridspec_kw={'height_ratios': [5, 1]})

    errors_d_top.boxplot(ax=ax)
    errors_d_top.boxplot(ax=ax2)

    ax.set_ylim(-1.1, 1.1)
    ax2.set_ylim(-4.7,-4.3)
    
    # hide the spines between ax and ax2
    ax.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    
    ax2.set_xticks([1, 2, 3])
    #ax.set_xticks([])
    f.subplots_adjust(hspace=0.1)
    
    d = .015 # how big to make the diagonal lines in axes coordinates
    kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
    ax.plot((1-d,1+d), (-d,+d), **kwargs)
    ax.plot((-d,+d),(-d,+d), **kwargs)
    
    kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
    ax2.plot((-d,+d), (1-d,1+d), **kwargs)
    ax2.plot((1-d,1+d), (1-d,1+d), **kwargs)
    
    ax2.set_xlabel('Fit method')
    ax.set_ylabel('Relative error')
    ax.set_title("Boxplot of relative diameter error using\ndifferent fit methods on topology data")
    
    plt.savefig("top_d_error.png", dpi=200)

def grav_d_error_plot():
    errors_d_grav.boxplot()
    plt.xlabel('Fit method')
    plt.ylabel('Relative error')
    plt.title("Boxplot of relative diameter error using\ndifferent fit methods on gravity data")
    plt.savefig("grav_d_error.png", dpi=200)
    plt.show()
    
def grav_abs_d_error_plot():
    errors_d_grav.abs().boxplot()
    plt.xlabel('Fit method')
    plt.ylabel('Absolute error')
    plt.title("Boxplot of absolute diameter error using\ndifferent fit methods on gravity data")
    plt.savefig("grav_abs_d_error.png", dpi=200)
    plt.show()
    
def top_abs_d_error_plot():
    f,(ax,ax2) = plt.subplots(2,1,sharex=True, facecolor='w', gridspec_kw={'height_ratios': [1, 5]})

    errors_d_top.abs().boxplot(ax=ax)
    errors_d_top.abs().boxplot(ax=ax2)

    ax2.set_ylim(-0.05, 0.95)
    ax.set_ylim(4.3,4.7)
    
    # hide the spines between ax and ax2
    ax.spines['bottom'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    
    ax2.set_xticks([1, 2, 3])
    #ax.set_xticks([])
    f.subplots_adjust(hspace=0.1)
    
    d = .015 # how big to make the diagonal lines in axes coordinates
    kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
    ax.plot((1-d,1+d), (-d,+d), **kwargs)
    ax.plot((-d,+d),(-d,+d), **kwargs)
    
    kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
    ax2.plot((-d,+d), (1-d,1+d), **kwargs)
    ax2.plot((1-d,1+d), (1-d,1+d), **kwargs)
    
    ax2.set_xlabel('Fit method')
    ax2.set_ylabel('Absolute error')
    ax.set_title("Boxplot of absolute diameter error using\ndifferent fit methods on topology data")
    plt.savefig("top_abs_d_error.png")
    
   