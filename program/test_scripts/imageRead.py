#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 13 12:28:20 2020

@author: Sindre Aalhus (sia20@aber.ac.uk)
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt

path = '../../SLDEM2015_128_60S_60N_000_360_FLOAT.IMG'  
path2 = './../gggrx_0900c_boug_l600.img'
    
#from https://riptutorial.com/python/example/24382/shifting-a-list-using-slicing
def shift_list(array, shift):
    """
    Shifts a list 
    Parameters
    ----------
    array : list
        The list to shift
    shift : int
        The amout to shift by. Positive number = shift right, negative = shift left.

    Returns 
    -------
    list : The shifted list
    """
    shift %= len(array)
    shift *= -1
    return array[shift:] + array[:shift]


def to_uint8(d0):
    """
    Converts the float32 image list to a uint8 2d list
    Parameters
    ----------
    d0 : 1d list
        The data from file

    Returns
    -------
    list : the data normalized to 0 - 255 and reshaped as per the LBL file

    """
    norm = cv2.normalize(d0, None, 2**8, 0, cv2.NORM_MINMAX)
    norm.shape = (15360, 46080)
    return np.rint(norm).astype(np.uint8)

def _canny(d0):
    canny_img = cv2.Canny(d0, 0, 10, 20)
    return canny_img

def _laplacian(d0):
    lap_img = cv2.Laplacian(d0, ddepth=cv2.CV_32F)    
    return lap_img

def edges(d0):
    d0 = d0.astype(np.float32)
    edge = np.abs(d0 - cv2.GaussianBlur(d0, (127, 127), 0))
    cv2.imwrite('edge.png', edge)
    return edge
    
    
def gaus(d0):
    """
    Generates the gaussian of an image and subtracts it, attemting to create a 
    edge image

    Parameters
    ----------
    d0 : list 2d
        The image

    Returns
    -------
    list : the edge image

    """
    norm = to_uint8(d0)
    #d0.shape = (15360, 46080)
    norm = norm[5000:, 28000:40000]
    #norm = cv2.normalize(norm, None, 2**8, 0, cv2.NORM_MINMAX)
    #norm = np.rint(norm).astype(np.uint8)
    blur = cv2.GaussianBlur(norm, (43, 43), 0)
    diff = np.abs(norm - blur) #subtract form original image to get edges, make all values positive
    norm_diff = cv2.normalize(diff, None, 2**8, 0, cv2.NORM_MINMAX) #normalize between 0-255
    return norm_diff
                
def plot_line(d0, line, ksize=None):
    """
    Plots the values across a line of the image

    Parameters
    ----------
    d0 : list
        The image
    line : int
        The line number

    Returns
    -------
    None
    """
    line_values = d0[line, :]
    gradient = np.gradient(line_values)
    plt.plot(range(len(line_values)), line_values)
    if (ksize):
           plt.title('K-size = ' + str(ksize))
    plt.show()
    plt.plot(range(len(line_values)), gradient)
    if (ksize):
           plt.title('K-size = ' + str(ksize))
    plt.show()
    return line_values

if (__name__ == "__main__"):
    with open(path, 'rb') as f:
        #d0 = np.fromfile(f, 'f4')
        d = np.fromfile(f)
        norm = cv2.normalize(d, None, 2**8, 0, cv2.NORM_MINMAX)
        norm = np.rint(norm).astype(np.uint8)
        heatmap = cv2.applyColorMap(norm, cv2.COLORMAP_RAINBOW)
        heatmap.shape = (15360, 23040, 3)
        cv2.imwrite('bouger.png', heatmap)
        
        #y = 5000
        #u8 = to_uint8(d0)[5000:, 28000:40000]
        #k_size = (2**8)-1
        #img = cv2.GaussianBlur(u8, (k_size, k_size), 0)        
        #line_values = plot_line(img, y, k_size)
        #print(line_values.index(min(line_values)))
        #print(2**8, '\t done!')
        #size = np.argmax(line_values) - np.argmin(line_values)
        #cv2.circle(img, (int(np.mean(np.where(line_values <= (np.min(line_values)+11)))), 5000), abs(size), 255, 3)
        #cv2.imwrite('circle.png', img)
        
        #for i in range(1, 9):
            #k_size = (2**i)-1
            #img = cv2.GaussianBlur(u8, (k_size, k_size), 0)        
            #plot_line(img, y, k_size)
            #print(2**i, '\t done!')
            #cv2.line(img, (0, y), (12000, y), 255, 5)
            #cv2.imwrite('image_line_ ' + str(k_size) + '.png', img)
        
