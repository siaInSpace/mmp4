"""
Multiple test scripts used to test new features for the master project.

Note that functions in this part of the module is not documented, however webpages will be created for each and can give an overview over the functions available. For more information please refer to the source code.
"""
__version__ = '0.1' 
__author__ = 'Sindre Aalhus'
__date__ = "2021-05-15"
