#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Holds test investigative models, fitted_peak3 and scipy_signal2."""
__date__ = "2021-04-29"
__author__ = "Sindre Aalhus"

import numpy as np
import matplotlib.pyplot as plt
import lmfit
import tqdm
import scipy
#Add path to my classes
import sys
package_path = '/home/saa/Documents/mmp4'
if (not package_path in sys.path):
    sys.path.append(package_path)
import program.Crater_tools.Crater as Crater
import program.Crater_tools.pyshtools_read as pysh_r
import uncertainties
plt.rcParams.update({'font.size': 20})




def capped_gaussian(x, amplitude=1.0, center=0.0, sigma=10.0, max_val=np.Inf):
    """Gaussian function capped at ceilieng max_val."""
    return np.clip(lmfit.lineshapes.lorentzian(x, amplitude, center, sigma), 
                   None, max_val)


def fitted_peak3(data):
    """Fit method, uses a capped gaussian for peak and quadratic model for background data."""
    peak = lmfit.Model(capped_gaussian, prefix='peak', independent_vars=['x'])
    peak = lmfit.models.LorentzianModel(prefix='peak')
    background = lmfit.models.QuadraticModel(prefix='background')

    max_pos = np.where(data == np.amax(data))[0][0]
    amp = np.abs(np.max(data)-np.min(data))
    #print(amp)
    #print(max_pos)
    params = peak.make_params(amplitude=amp, center=max_pos, max_val=amp)
    params += background.make_params(c=np.min(data))
    
    print(params)
    
    model = peak + background
    x = np.arange(len(data))
    return model.fit(data, params,x=x)
    

def get_peak_val(rec_model):
    """Use the parameter of a fitted model to find the peak value."""
    params = rec_model.best_values
    amp = params['peakamplitude']
    c1 = params['peakcenter1']
    s1 = params['peaksigma1']
    c2 = params['peakcenter2']
    s2 = params['peaksigma2']
    x = (c1+c2)/2.0
    
    if (x > 512 or x < 0):
        #x is out of bounds, set to center of data
        x = 255
        
    return lmfit.lineshapes.rectangle(x, amp, c1, s1, c2, s2, 'arctan')

def most_prominent(peaks, n=1):
    """
    Sort peaks by prominence and extract the n most prominent.

    Parameters
    ----------
    peaks : tuple
        Result from scipy.signal.find_peaks, must conatin prominence.
    n : int, optional
        The number of prominences to extract. The default is 1.

    Returns
    -------
    List of the n most prominent resutls.
    """
    sorted_peaks = sorted(zip(peaks[1]['prominences'], peaks[0]), reverse=True)
    if (len(sorted_peaks) < n):
        raise IndexError("Cannot extract {} most prominent peaks from only {} peaks".format(n, len(sorted_peaks)))
    return sorted_peaks[:n]



def scipy_signal2(data, min_distance=0):
    """
    Get the most prominent peak as well as the most prominent peak either side of up side down data.

    Parameters
    ----------
    data : numpy.ndarray
        The data to extract peaks from.

    Returns
    -------
    center_peak : list
        Prominence and index of most prominent peak.
    left_peak : list
        Prominence and index of most prominent upside down peak left of center_peak.
    right_peak : list
        Prominence and index of most prominent upside down peak right of center_peak.
    """
    up = scipy.signal.find_peaks(data, prominence=1, distance=30)
    center_peak = most_prominent(up)[0]

    #Invert data (upside down) to find the location of the dips
    upside_down_data = data*-1
    down = scipy.signal.find_peaks(upside_down_data, prominence=1, distance=30)
    
    #unpack peaks into array
    peaks = np.array(list(zip(down[1]['prominences'], down[0])))
    
    #Find left peaks (peaks with index less than center peak)
    left_peaks = peaks[peaks[:,1] < center_peak[1]-min_distance]
    #repack left peak
    tmp = {}
    tmp['prominences'] = left_peaks[:,0]
    left_peaks = (left_peaks[:,1], tmp)
    
    #Find right peaks (peaks with index greater or equal to center peak)
    right_peaks = peaks[peaks[:,1] >= center_peak[1]+min_distance]
    #repack peaks
    tmp = {}
    tmp['prominences'] = right_peaks[:,0]
    right_peaks = (right_peaks[:,1], tmp)
    
    
    # #Split into left and right side as not to get two peaks on one side
    # #Added extra padding as a result of noisy peaks causing side peaks to register centre peak twice
    # up_down_left = upside_down_data[:center_peak[1]]
    # up_down_right = upside_down_data[center_peak[1]:]
    
    # #Find the peaks
    # left_dip = scipy.signal.find_peaks(up_down_left, prominence=1, distance=30)
    # right_dip = scipy.signal.find_peaks(up_down_right, prominence=1, distance=30)

    #Only get the most prominent peaks, [0] to remove sorrounding list
    left_peak = most_prominent(left_peaks, 1)[0]
    right_peak = most_prominent(right_peaks, 1)[0]
      
    return (center_peak, left_peak, right_peak)
    
def crater_scipy_signal2(c_map, crater):
    """Use the scipy_signal2 method on data from the given crater using the supplied crater_map (c_map)."""
    try:
        d, s, px_km  = c_map.get_crater_map(crater, 512, True)
    except:
        print("No crater")
        return None
    crater_analyser = pysh_r.Crater_analyser(d, s, px_km)
            
    if (type(crater_analyser.crater_data) == type(None)):
        print("No analyser")
        return None
    fits = []
    for i in range(0, 180, 1):
        fits.append(crater_analyser.find_peaks(np.array, i))
    fits = np.array(fits)
    # medians = np.median(fits, 0)
    means = np.mean(fits, 0)
    return scipy_signal2(means, 20)
    


def plot_scipy_signal2(data, peaks):
    """
    Use index from peaks to extract y value from data and plots and scatters.

    Parameters
    ----------
    data : numpy.ndarray
        The data to be plotted.
    peaks : tuple
        Result from scipy_signal2.

    Returns
    -------
    None.
    """
    #Unpack peaks
    (center_peak, left_peak, right_peak) = peaks
    #Get index values
    x_s0 = int(center_peak[1])
    x_s1 = int(left_peak[1])
    x_s2 = int(right_peak[1])
    x = [x_s0, x_s1, x_s2]
    
    #Extract y value
    y = data[x]
    
    plt.plot(data, zorder=-1)
    plt.scatter(x, y, marker="D", c=["r", "indigo", "indigo"])
    
def scipy_signal2_get_height_and_width(data, scale=1, px_km=1, min_distance=0):
    """
    Get height and width from data and peak.
    
    Parameters
    ----------
    data : TYPE
        DESCRIPTION.
    peaks : TYPE
        DESCRIPTION.

    Returns
    -------
    height : float
        Average y of left and right peak and subtracts from center peak.
    width : int
        Index distance of left and right peak.

    """
    c_peak, l_peaks, r_peak = scipy_signal2(data, min_distance)
    #Unpack peaks
    # (center_peak, left_peak, right_peak) = peaks
    #Extract y values and calulate height
    y_center = data[int(c_peak[1])]
    y_left = data[int(l_peaks[1])]
    y_right = data[int(r_peak[1])]
    height = y_center - (y_left + y_right)/2.0
    
    width = (r_peak[1]-l_peaks[1])/scale/px_km
    w = uncertainties.ufloat(width, 1/scale/px_km)
    h = uncertainties.ufloat(height, np.std([y_left, y_right]))
    return (h, w)


def new_plot(data_entry):
    """Plot the resulting index values from scipy_signal2. Uses a the results from the fit_one funciton in the main script."""
    name, (fit, scale, px_km, data2d) = data_entry
    c_peak, l_peaks, r_peak = scipy_signal2(fit.data)
    x = np.arange(len(fit.data))
    side_len = len(fit.data)/2/scale/px_km #km
    x = np.linspace(-side_len, side_len, len(fit.data)) 
    xlabel = "distance from center (km)"

    fig=plt.figure(figsize=(10.6, 6))
    # fig, (ax1, ax2) = plt.subplots(1, 2, sharey=False, figsize=(14.2, 8))
    plt.plot(x, fit.data, 'b', label='Data', zorder=-1)
    plt.plot(x, fit.best_fit, 'r-', label='Best fit', zorder=-1)
    
    x_s0 = c_peak[1]
    x_s1 = l_peaks[1]
    x_s2 = r_peak[1]
    x_peaks = np.array([x_s0, x_s1, x_s2], int)
    y = fit.data[x_peaks]
    plt.scatter((np.array(x_peaks)-255)/scale/px_km, y, marker="D", c=["r", "indigo", "indigo"])

    plt.legend(loc=1)    
    plt.title(name)   
    plt.xlabel(xlabel)
    plt.ylabel("Bouguer anomaly (mGal)")
    plt.tight_layout()


if (__name__ == '__main__'):
    pass
    
    