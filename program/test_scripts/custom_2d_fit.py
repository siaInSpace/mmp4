#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 20:21:47 2021

@author: saa
"""

import numpy as np
import matplotlib.pyplot as plt
import lmfit
#Add path to my classes
import sys
package_path = '/home/saa/Documents/mmp4'
if (not package_path in sys.path):
    sys.path.append(package_path)
import program.Crater_tools.Crater as Crater
import program.Crater_tools.pyshtools_read as pysh_r
import concurrent.futures as  futures
from time import sleep



def step(data,  amp, c1, c2, s1, s2):
    a1 = (data - c1)/s1
    a2 = -(data - c2)/s2
    return amp * (np.arctan(a1) + np.arctan(a2)) / np.pi

def step2(x, y, amp=1, x_c1=0, x_c2=0, x_s1=1, x_s2=1, y_c1=0, y_c2=0, 
          y_s1=1, y_s2=1):
    return amp * step(x, 1, x_c1, x_c2, x_s1, x_s2) * step(y, 1, y_c1,
                                                           y_c2, y_s1, y_s2)

def quad(x, a, b, c):
    return a * x**2 + b * x + c

def quad2(x, y, a=-1, b=-1, c=0, d=0, e=0, f=0):
    return a*x**2 + b*y**2 + c*x*y + d*x + e*y + f

def test_step2():
    x = np.linspace(0, 512, 512)
    y = np.linspace(0, 512, 512)
    X, Y = np.meshgrid(x, y)
    d = step2(X, Y, 10, 128, 255, 50, 50, 128, 255, 50, 50)
    plt.imshow(d)
    
    
def lorentzian2d(x, y, amplitude=1., centerx=0., centery=0., sigmax=1., sigmay=1.,
                 rotation=0, max_val=np.Inf):
    xp = (x - centerx)*np.cos(rotation) - (y - centery)*np.sin(rotation)
    yp = (x - centerx)*np.sin(rotation) + (y - centery)*np.cos(rotation)
    R = (xp/sigmax)**2 + (yp/sigmay)**2

    return np.clip(2*amplitude*lmfit.lineshapes.lorentzian(R)/(np.pi*sigmax*sigmay), None, max_val)


def capped_gauss2d(x, y=0.0, amplitude=1.0, centerx=0.0, centery=0.0, 
                   sigmax=1.0, sigmay=1.0, max_val=np.Inf, offset=0.0):
    return np.clip(lmfit.lineshapes.gaussian2d(x, y, amplitude, centerx, 
                                   centery, sigmax, sigmay)+offset, None, max_val)



def new_main(data, shape=512):
    gauss1 = lmfit.Model(capped_gauss2d, independent_vars=['x', 'y'], prefix='peak_')
    
    s = np.linspace(0, shape, shape)
    X, Y = np.meshgrid(s, s)
    
    # use_grav=True
    # shape=512
    # c_map = pysh_r.Crater_map(create_top=True, create_grav=use_grav)
    # crater = c_map.craters[key]
    
    # #custom_crater = Crater.Crater("custom") 
    # #custom_crater.add_source(Crater.Crater_source(80, 60, 400)) #lon, lat, d
    
    # # crater = custom_crater
    
    # d, s, px_km  = c_map.get_crater_map(crater, shape, use_grav)
    
    p = gauss1.make_params(amplitude=data.max()*100, centerx=255, centery=255, sigmax=50, sigmay=50, max_val=data.max(), offset=data.min())
    return gauss1.fit(data, params=p, x=X, y=Y)

def main():
    
    lm_lorentz2 = lmfit.Model(lorentzian2d, independent_vars=['x', 'y'], prefix='l_')
    c = lmfit.models.ConstantModel(prefix='c_')
   
    
    s = np.linspace(0, 512, 512)
    X, Y = np.meshgrid(s, s)
    
    use_grav=True
    key='Orientale'
    shape=512
    c_map = pysh_r.Crater_map(create_top=True, create_grav=use_grav)
    crater = c_map.craters[key]
    
    #custom_crater = Crater.Crater("custom") 
    #custom_crater.add_source(Crater.Crater_source(80, 60, 400)) #lon, lat, d
    
    # crater = custom_crater
    
    d, s, px_km  = c_map.get_crater_map(crater, shape, use_grav)
    
    p = lm_lorentz2.make_params(amplitude=d.max()-d.min(), centerx=255, centery=255, sigmax=50, sigmay=50, max_val=d.max())

    p += c.make_params(c=d.min())
    p['l_amplitude'].min = 0
    #p['l_amplitude'].max = 10000
    p['l_centerx'].min = 150
    p['l_centerx'].max = 350
    p['l_centery'].min = 150
    p['l_centery'].max = 350
    p['l_sigmax'].max = 1000
    p['l_sigmax'].min = -1000
    p['l_sigmay'].max = 1000
    p['l_sigmay'].min = -1000

    mod = lm_lorentz2 + c # + lm_step2
    out = mod.fit(d, params=p, x=X, y=Y)
    return out



if (__name__ == '__main__'):
    pass
    # o = new_main()
    # pool = futures.ProcessPoolExecutor(3)
    # future = pool.submit(main)
    # while(not future.done()):
    #     print("Not done, Sleeping 10")
    #     sleep(10)
    # print("Done")

