#!/usr/bin/python

import queue as Queue
import threading
import time

exitFlag = 0
n_args = 12

threadList = list(range(n_args))
nameList = list(range(n_args))

queueLock = threading.Lock()
workQueue = Queue.Queue(n_args)
threads = []
threadID = 1


class myThread (threading.Thread):
   def __init__(self, threadID, name, q):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
      self.q = q
   def run(self):
      print("Starting " + self.name)
      process_data(self.name, self.q)
      print("Exiting " + self.name)

def process_data(threadName, q):
   while not exitFlag:
      queueLock.acquire()
      if not workQueue.empty():
          data = q.get()
          queueLock.release()
          print("{} processing {}".format(threadName, data))
          time.sleep(5)
      else:
         queueLock.release()
      time.sleep(1)

if (__name__ == "__main__"):
    # Create new threads
    for tName in threadList:
        thread = myThread(threadID, tName, workQueue)
        thread.start()
        threads.append(thread)
        threadID += 1

    # Fill the queue
    queueLock.acquire()
    for word in nameList:
        workQueue.put(word)
    queueLock.release()

    start  = time.time()

    # Wait for queue to empty
    while not workQueue.empty():
        pass

    # Notify threads it's time to exit
    exitFlag = 1

    # Wait for all threads to complete
    for t in threads:
        t.join()
        print("Exiting Main Thread")
        print("Time: {}".format(time.time()-start))
