#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Fit Two Dimensional Peaks
=========================
This example illustrates how to handle two-dimensional data with lmfit.
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import griddata
import scipy.optimize as opt
import lmfit
from lmfit.lineshapes import gaussian2d, lorentzian

"""
###############################################################################
# Two-dimensional Gaussian
# ------------------------
# We start by considering a simple two-dimensional gaussian function, which
# depends on coordinates `(x, y)`. The most general case of experimental
# data will be irregularly sampled and noisy. Let's simulate some:
npoints = 10000
x = np.random.rand(npoints)*10 - 4
y = np.random.rand(npoints)*10 - 4

z = gaussian2d(x, y, amplitude=30, centerx=100, centery=100, sigmax=.5, sigmay=.5)

z += 2*(np.random.rand(*z.shape)-.5)
error = np.sqrt(z+1)
###############################################################################
# To plot this, we can interpolate the data onto a grid.
X, Y = np.meshgrid(np.linspace(x.min(), x.max(), 512),
                   np.linspace(y.min(), y.max(), 512))
Z = griddata((x, y), z, (X, Y), method='linear', fill_value=0)

#fig, ax = plt.subplots()
#art = ax.pcolor(X, Y, Z, shading='auto')
#plt.colorbar(art, ax=ax, label='z')
#ax.set_xlabel('x')
#ax.set_ylabel('y')
#plt.show()
###############################################################################
# In this case, we can use a built-in model to fit
model = lmfit.models.Gaussian2dModel()
params = model.guess(z, x, y)
result = model.fit(z, x=x, y=y, params=params, weights=1/error)
lmfit.report_fit(result)
###############################################################################
# To check the fit, we can evaluate the function on the same grid we used
# before and make plots of the data, the fit and the difference between the two.
fig, axs = plt.subplots(1, 2, figsize=(10, 10))

vmax = np.nanpercentile(Z, 99.9)

ax = axs[0]
art = ax.pcolor(X, Y, Z, vmin=0, vmax=vmax, shading='auto')
plt.colorbar(art, ax=ax, label='z')
ax.set_title('Data')

ax = axs[1]
fit = model.func(X, Y, **result.best_values)
art = ax.pcolor(X, Y, fit, vmin=0, vmax=vmax, shading='auto')
plt.colorbar(art, ax=ax, label='z')
ax.set_title('Fit')

#ax = axs[1, 0]
#fit = model.func(X, Y, **result.best_values)
#art = ax.pcolor(X, Y, Z-fit, vmin=0, vmax=10, shading='auto')
#plt.colorbar(art, ax=ax, label='z')
#ax.set_title('Data - Fit')

for ax in axs.ravel():
    ax.set_xlabel('x')
    ax.set_ylabel('y')
#axs[1, 1].remove()
plt.show()
"""

def twoD_Gaussian(xdata_tuple, amplitude, xo, yo, sigma_x, sigma_y, theta, offset):
    (x, y) = xdata_tuple                                                        
    xo = float(xo)                                                              
    yo = float(yo)                                                              
    a = (np.cos(theta)**2)/(2*sigma_x**2) + (np.sin(theta)**2)/(2*sigma_y**2)   
    b = -(np.sin(2*theta))/(4*sigma_x**2) + (np.sin(2*theta))/(4*sigma_y**2)    
    c = (np.sin(theta)**2)/(2*sigma_x**2) + (np.cos(theta)**2)/(2*sigma_y**2)   
    g = offset + amplitude*np.exp( - (a*((x-xo)**2) + 2*b*(x-xo)*(y-yo)         
                        + c*((y-yo)**2)))                                   
    return g.ravel()

if (__name__ == "__main__"):
    # Create x and y indices
    x = np.linspace(0, 512, 512)
    y = np.linspace(0, 512, 512)
    x, y = np.meshgrid(x, y)

    #create data
    data = twoD_Gaussian((x, y), 500, 255, 255, 80, 80, 0, -400)

    # plot twoD_Gaussian data generated above
    plt.figure()
    plt.imshow(data.reshape(512, 512))
    plt.colorbar()

    #add some noise to the data and try to fit the data generated beforehand
    initial_guess = (500, 255, 255, 80, 80, 0, 00)

    data_noisy = data + 0.2*np.random.normal(size=data.shape)

    popt, pcov = opt.curve_fit(twoD_Gaussian, (x, y), data_noisy, p0=initial_guess)

    data_fitted = twoD_Gaussian((x, y), *popt)

    fig, ax = plt.subplots(1, 1)
    #ax.hold(True)
    ax.imshow(data_noisy.reshape(512, 512), cmap='gray',
        extent=(x.min(), x.max(), y.min(), y.max()))
    ax.contour(x, y, data_fitted.reshape(512, 512), 8, colors='w')
    plt.show()
