#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Inspired by tutorials from the pyshtools website.

https://nbviewer.jupyter.org/github/SHTOOLS/SHTOOLS/blob/master/examples/notebooks/gravity-and-magnetic-fields.ipynb
and
https://nbviewer.jupyter.org/github/SHTOOLS/SHTOOLS/blob/master/examples/notebooks/plotting-maps.ipynb
Holds multiple sources to handle pyshtool objects and other related objects.
Sperhical harmonic models are representaed using the pyshtools class SHRealCoeffs and will be refered to as Coeffs.
"""

__date__ = "2020-10-30"
__author__ = "Sindre Aalhus"

import matplotlib.pyplot as plt
import numpy as np
import cv2
import scipy.ndimage.filters 
import scipy.signal
import astropy.constants
import pyshtools as pysh
from scipy.signal import butter,filtfilt
#Add path to my classes
import sys
package_path = '/home/saa/Documents/mmp4'
if (not package_path in sys.path):
    sys.path.append(package_path)
import program.Crater_tools.Crater as Crater

def butter_lowpass_filter(data, cutoff=2, fs=30, order=2):
    """
    Butterworth filter.

    Parameters
    ----------
    data : numpy.ndarray
        The data to filter.
    cutoff : float, optional
        Cutoff frequency (Hz). The default is 2.
    fs : float, optional
        Sample rate (Hz). The default is 30.
    order : int, optional
        Sine wave approximation. The default is 2.

    Returns
    -------
    y : numpy.ndarray
        The filtered data.

    """
    nyq = 0.5 * fs  # Nyquist Frequency

    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y


class Crater_map:
    """Handles the convertion from pyshtools to array."""
    
    lmax_t = 2600
    lmax_g = 1200
    
    @staticmethod
    def get_topology(lmax=1000):
        """
        Get the topology Coeffs in km.

        Parameters
        ----------
        lmax : int, optional
        The order and degree used for sperhical harmonic calculations. The default is 1000.

        Returns
        -------
        clm : pysh.SHRealCoeffs
            The sperhical harmonic model used to represent the topology.

        """
        #Get topology map and convert from m to km.
        clm = pysh.datasets.Moon.MoonTopo2600p(lmax=lmax) / 1e3 
        clm.units = 'km'
        clm.coeffs[0,0,0] = 0. #degree = 0
        clm.coeffs[0,2,0] = 0. #flattening = 0
        return clm
    
    @staticmethod
    def get_gravity(lmax=1000):
        """
        Get the bouguer anomaly Coeffs in mGal. Used gravity measurements and topology data to calculate.
        
        Parameters
        ----------
        lmax : int, optional
             The order and degree used for sperhical harmonic calculations. The default is 1000.

        Returns
        -------
        pysh.SHRealCoeffs
            The sperhical harmonic model used to represent the bouguer anomaly.

        """
        # Get graviy data
        # clm_grav = pysh.datasets.Moon.GRGM1200B(lmax=lmax)
        # clm_grav.omega = 0
        # #Convert from pysh.SHGravCoeffs to pysh.SHRealCoeffs.
        # grav_xarray = clm_grav.expand().to_xarray()
        # gravity_disturbance = pysh.SHGrid.from_xarray(grav_xarray.total).expand() * 1e5 # Convert to mGal
        # gravity_disturbance.units = 'mGal'
        
        c = pysh.datasets.Moon.GRGM1200B(lmax=lmax)
        g = c.expand(normal_gravity=False) #Normal gravity calulation causeing long run time
        x = g.to_xarray().total #Get data in array form
        d = x.data - pysh.constants.Moon.g0.value #Manually remove normal gravity to get gravity disturbance
        filtered_d = butter_lowpass_filter(d) #Filter data to remove noise from higher order sperhical harmonic model
        g1 = pysh.SHGrid.from_array(filtered_d) #Wrap data into pyshtools grid
        gravity_disturbance = g1.expand() * 1e5 #Recalculate coefs for SHCoeffs and Convert to mGal
        gravity_disturbance.units = 'mGal'
        
        #calculate bouguer correction
        g = astropy.constants.G.value * 2 * np.pi * 1e5 # mGal m^2 kg^-1
        mGal_m = g * 2560 #from grail2 mGal m^-1
        clm_top = Crater_map.get_topology(lmax=lmax) * 1e3 #m
        b_corr = clm_top * mGal_m
        b_corr.units = 'mGal'
          
        #return bouguer anomaly
        return gravity_disturbance - b_corr

    def __init__(self, l_t=1000, l_g=1000, create_top=True, create_grav=False):
        """
        Initilize object.

        Parameters
        ----------
        l_t : int, optional
            Degree and order for topology calculations. The default is 1000.
        l_g : TYPE, optional
            Degree and order for gravity calculations. The default is 1000.
        create_top : bool, optional
            Wheter or not to create topology map. The default is True.
        create_grav : bool, optional
            Wheter or not to create gravity map. The default is False.

        Raises
        ------
        ValueError
            If l_t or l_g is out of bounds, determined by lmax_t and lmax_g.

        Returns
        -------
        None.

        """
        #Check that l_t and l_g are in bounds
        if (l_t > self.lmax_t):
            raise ValueError("l_t({}) too high, must be less than lmax_t:{}".format(l_t, self.lmax_t))
        if (l_g > self.lmax_g):
            raise ValueError("l_g({}) too high, must be less than lmax_g:{}".format(l_g, self.lmax_g))
            
        self.l_t = l_t
        self.l_g = l_g
        self.craters = Crater.get_craters()
        
        if (create_top):
            self.topology_map = Crater_map.get_topology(l_t)
        if(create_grav):
            self.gravity_map = Crater_map.get_gravity(l_g)
        
    def rotate_to_crater(self, crater, clm, offset_lon=0.0, offset_lat=0.0):
        """
        Rotate Coeffs to a craters location.

        Parameters
        ----------
        crater : Crater.Crater
            A Crater object from my Crater class.
        clm : pysh.RealCoeffs
            The Coeffs to roate.
        offset_lon : float, optional
            lontitude offset. The default is 0.0.
        offset_lat : float, optional
            Lattitude offset. The default is 0.0.

        Returns
        -------
        pysh.SHRealCoeffs.
            The rotated Coeffs.

        """
        lon = crater.sources[0].lon + offset_lon
        lat = crater.sources[0].lat + offset_lat
        #First rotates crater center to 0 deg E, then raise this to 0deg N, finally move to center (180E)
        return clm.rotate(lon, -lat, 180)
    
        
    def get_crater_map(self, key, resize=None, gravity=False):    
        """
        Projects the Coeffs using a simple cyrindical projection to a 2d array and extracts the area surrounding a crater.

        Parameters
        ----------
        key : str or Crater.Crater
            Either a dict key to use with this objects craters variable or a new Crater object.
        resize : int, optional
            If given will rezise the data. The default is None.
        gravity : bool, optional
            Uses Topology if False otherwise use Gravity data. The default is False.

        Raises
        ------
        AttributeError
            If the key cannot be used with this objects dictonary and key is not a Crater.Crater object.

        Returns
        -------
        resized_data : numpy.ndarray
            The data sourounding the data.
        scale: float
            The scale used to resize the data. (pixel per pixel)
        px_km : float
            How many px per km in the data

        """
        #Check if key can be used in dictonary
        try:
            crater = self.craters[key]
        except KeyError:
            #if not check if it is a crater object
            if (isinstance(key, Crater.Crater)):
                crater = key
            else:
                #if neither raise error
                raise AttributeError("key is not valid, not in dict and not a crater object. (key: {}) ".format(key))    

        #Select gravity or topology map
        clm = None
        try:
            if (gravity):
                clm = self.gravity_map 
            else:
                clm = self.topology_map
        except AttributeError as e:
            #Raised if selected map is not created by object.
            #Possiblty could be created on request.
            print(e)
            return None
        #Rotate to crater.
        rotated_topology_map = self.rotate_to_crater(crater, clm)
            
        #Convert to array
        ar = (rotated_topology_map.expand().to_array())
        
        #Find crater center in array
        len_y, len_x = ar.shape
        px_km = len_x / ((pysh.constants.Moon.r.value / 1000)*2 * np.pi) #circumfrence_Moon ~10914.8km
        
        #Get crater diameter from its sources.
        d = 0
        for s in crater.sources:
            d = s.diameter
            if d != 0:
                break
        if d == 0:
            print("No diameter")
            #Perhaps raise error instead
            return None
        d_x = px_km * (d * 2) # determince size to extract (2*diameter of crater)
        centre_x = int(len_x / 2)
        centre_y = int(len_y / 2)
        
        #Extract only area around crater center        
        data = ar[centre_y - int(d_x/2) : centre_y + int(d_x/2), centre_x - int(d_x/2) : centre_x + int(d_x/2)]

        if (not resize):
            return (data, 1, px_km)
        #print(data.shape)
        
        #resize data
        resized_data = cv2.resize(data, (resize, resize))
        scale =  float(resize) / float(data.shape[0])
        #print(scale)
        return (resized_data, scale, px_km)
    
    @staticmethod
    def plot_all_craters(lmax=2600, prefix="", suffix=""):
        """
        Save an image of each crater in this objects craters dict.

        Parameters
        ----------
        lmax : int, optional
            Degree and order for Coeffs calculations.. The default is 2600.
        prefix : str, optional
            prefix for save location. The default is "".
        suffix : str, optional
            suffix for file name. The default is "".

        Returns
        -------
        None.

        """
        crater_map = Crater_map()
        for key in sorted(crater_map.craters.keys()):
            temp_map = crater_map.get_crater_map(key, 512)
            try:
                plt.imsave('plots/' + key + '.png', temp_map, cmap='gray')
            except AttributeError:
                print("Error, trying next")
                    


class Crater_map_handler:
    """Handles some plotting and type convertion of Crater maps."""
    
    def __init__(self, crater_map, scale, px_km):
        self.crater_map = crater_map
    
    @staticmethod
    def to_uint8(data):
        """
        Normalize data between 0 and 2**8 and converts type to uint8.

        Parameters
        ----------
        data : numpy.ndarray
            The data to convert.

        Returns
        -------
        numpy.ndarray
            The data normalized and converted to uint8.

        """
        #Normalize
        data = cv2.normalize(data, None, 2**8, 0, cv2.NORM_MINMAX)
        #Round and convert type
        return np.rint(data).astype(np.uint8)

    @staticmethod
    def to_uint16(data=None):
        """
        Normalize data between 0 and 2**16 and converts type to uint16.

        Parameters
        ----------
        data : numpy.ndarray
            The data to convert.

        Returns
        -------
        numpy.ndarray
            The data normalized and converted to uint16.

        """
        #Normalize
        data = cv2.normalize(data, None, 2**16, 0, cv2.NORM_MINMAX)
        #Round and convert type
        return np.rint(data).astype(np.uint16)
    
    def plot(self, cmap='gray'):
        """Convert to uint8 and plot map."""
        plt.figure()
        im = self.to_uint8()
        plt.imshow(im, cmap)
    
    def plot_line(self, row=None):
        """Get row from data and plot."""
        plt.figure()
        if (not row):
            row = self.crater_map.shape[0] / 2
            plt.plot(self.crater_map[int(row), :])
            plt.show()
            
    def get_crater_map(self):
        """Return crater map."""
        return self.crater_map
    
    
class Crater_analyser:
    """Functions to gather information of data around crater."""
    
    def __init__(self, crater_data, scale=1, px_km=1):
        self.crater_data = crater_data
        
    def get_gausian_edges(self, kernel_size, limit=None):
        """
        Subtracts blurred image from original to highlight edges.

        Parameters
        ----------
        kernel_size : int
            Size of the gausiann kernel.
        limit : int, optional
            If given will be used to threshold difference. The default is None.

        Returns
        -------
        diff : numpy.ndarray
            Edge image.

        """
        gaussian_data = scipy.ndimage.filters.gaussian_filter(self.crater_data, kernel_size)
        diff = np.absolute(gaussian_data - self.crater_data)
        #Values less than limit will be 0, over limit will be 1.
        if (limit):
            a = diff
            a[a>limit] = -1
            a[a>-1] = 1
            a[a==-1] = 0
        return diff

    def get_circles(self):
        """Attempt to fit circles to edge image usign Hough Circles."""
        edge_map = self.get_gausian_edges(37, 0.2)
        plt.imshow(edge_map, cmap='gray')
        edge_map = Crater_map_handler.to_uint8(edge_map)
        return cv2.HoughCircles(edge_map, cv2.HOUGH_GRADIENT, 1, 50,
                               param1=100, param2=30,
                               minRadius=25, maxRadius=100)
    
    def rotate_image(self, angle):
        """
        Roatate image along its center.

        Parameters
        ----------
        angle : float
            degrees to rotate.

        Returns
        -------
        numpy.ndarray
            Rotated data.

        """
        image_center = tuple(np.array(self.crater_data.shape[1::-1]) / 2)
        rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
        return cv2.warpAffine(self.crater_data, rot_mat, self.crater_data.shape[1::-1])

    def find_peaks(self, f_peaks, rotate=0.0, *args, **kwargs):
        """
        Find the peaks for this crater using supplied function.

        Parameters
        ----------
        rotate : float, optional
            The angle to rotate crater data. The default is 0.
        f_peaks : funciton
            The function to use to find peaks.
        *args : pointer to more arguments
            Arguments to pass to the function used.

        Returns
        -------
        Returns value(s) that were returned by f_peaks
        """
        data = self.crater_data
        if (not rotate%360 == 0):
            data = self.rotate_image(rotate)
        center_row = int(data.shape[0] / 2)
        y = data[center_row, :]
        return f_peaks(y, *args, **kwargs)
    
if (__name__ == '__main__'):
    pass
    # c = pysh.datasets.Moon.GRGM1200B(lmax=1000)
    # g = c.expand(normal_gravity=False) #Normal gravity calulation causeing long run time
    # x = g.to_xarray().total #Get data in array form
    # d = x.data - pysh.constants.Moon.g0.value #Manually remove normal gravity to get gravity disturbance
    # filtered_d = butter_lowpass_filter(d) #Filter data to remove noise from higher order sperhical harmonic model
    # g1 = pysh.SHGrid.from_array(d) #Wrap data into pyshtools grid
    # c1 = g1.expand() * 1e5 #Recalculate coefs for SHCoeffs and Convert to mGal
    # c1.units = 'mGal'