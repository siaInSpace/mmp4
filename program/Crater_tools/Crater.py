#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Multiple classes to hold information on a crater using multiple sources."""

__date__ = "2021-01-21"
__author__ = "Sindre Aalhus"

import csv
import numpy as np

def defualt_from_csv(row):
    """
    Load data from a csv row (list).

    Parameters
    ----------
    row : list
        The list containg data

    Returns
    -------
    name : string
        The crater name
    lon : float
        The longitude value
    lat : float
        The lattitude value
    d : float
        The diameter

    """
    # Attempt to standarize hyphen charachter
    try:
        name = row[0].replace('−', '-')
        name = name.strip()
        #name = name.replace(" ", "")
    except:
        name =""
    try:
        lat = ((float(row[1].replace('−', '-')) + 90) % 180) - 90
    except ValueError:
        lat = None
    try:
        lon = ((float(row[2].replace('−', '-')) + 180 ) % 360) - 180
    except ValueError:
        lon = None
    try:
        d = float(row[3].replace('−', '-'))
    except ValueError:
        d = None
        
    
    return (name, lon, lat, d) 


class Refrence:
    """
    Keeps limited data about refrence for later lookup.

    Attributes
    ----------
    author : str
        A string representing the name of the author(s)
    year : int
        The publishing year
    doi : str
        DOI to article
    """

    def __init__(self, author="", year=0, doi=""):
        self.author = author
        self.year = year
        self.doi = doi

    def __str__(self):
        """
        Return string reprensentation of object. (author in year from doi).

        Returns
        -------
        str
            String representing object.

        """
        return '{self.author} in {self.year} (doi: {self.doi})'.format(self=self)
    
    def __eq__(self, other):
        """
        Check if objects are equal, uses author, year and doi.

        Parameters
        ----------
        other : Refrence
            Object to test.

        Returns
        -------
        bool
            True if object have same varaibles.

        """
        if (self.author != other.author): return False
        if (self.year != other.year): return False
        if (self.doi != other.doi): return False
        return True
    
    def __ne__(self, other):
        """
        Check if objects are not equal, returns the invers of __eq__.

        Parameters
        ----------
        other : Refrence
            Object to test

        Returns
        -------
        bool
            False if object have same varables.

        """
        return not self == other

class Crater_source:
    """
    Holds information on a crater from a refrence.
    
    Attributes
    ----------
    lon : float
        Longitude position according to refrence
    lat : float
        Lattitude position according to refrence
    refrence : Refrence
        The refrence for the information in this object
    """

    def __init__(self, lon=0.0, lat=0.0, diameter=0.0, refrence=Refrence()):
        self.lon = lon
        self.lat = lat
        self.diameter = diameter
        self.refrence = refrence
        
      
    def __str__(self):
        """
        Generate string reprensation of crater source. (Lat, Lon, diameter, Refrence).

        Returns
        -------
        str
           String representation of object

        """
        return '({:02.02f}N, {:03.02f}E) {:04.02f}km [{}]'.format(self.lat, self.lon, self.diameter, self.refrence)
        #return "({}N, {}E) {}km [{}]".format(self.lat, self.lon, self.diameter, self.refrence)
    
    def __eq__(self, other):
        """
        Check if two objects are equal using lon, lat, diameter and refrence.

        Parameters
        ----------
        other : Crater_source
            Object to test.

        Returns
        -------
        bool
            True if attributes are equal

        """
        if (self.lon != other.lon): return False
        if (self.lat != other.lat): return False
        if (self.diameter != other.diameter): return False
        if (self.refrence != other.refrence): return False
        return True
    
    def __ne__(self, other):
        """
        Check iof two objects are not equal using inverse of __eq__.

        Parameters
        ----------
        other : Crater_source
            Object to test.

        Returns
        -------
        TYPE
            False if objects are equal

        """
        return not self == other
    
    def to_csv(self):
        """
        Generate string in csv format (lat, lon, diameter).

        Returns
        -------
        str
            The generated string.

        """
        return '{self.lat}, {self.lon}, {self.diameter}'.format(self=self)

   
        
class Crater:
    """
    Holds multiple sources for a crater.
    
    Attributes
    ----------
    name : str
        The name of the crater
    sources : list
        A list of the sources related to this craters
    """
    
    def __init__(self, name):
        if (not type(name) == str):
            raise TypeError("Name must be of type str (not: {})".format(type(name)))
        self.name = name
        self.sources = []
        
    def __str__(self):
        """
        Generate string reprensation of object by iterating through its sources.

        Returns
        -------
        temp_str : str
            The generated string

        """
        temp_str = self.name
        for s in self.sources:
            temp_str += '\n\t' + str(s)
        return temp_str

    def __repr__(self):
        """
        Return string used to represent object, same as __str__.

        Returns
        -------
        str
            output from __str__.

        """
        return self.__str__()

    def __contains__(self, item):
        """
        Check if source already exist in crater.

        Parameters
        ----------
        item : Crater_source
            The source to check

        Returns
        -------
        bool
        """
        return item in self.sources
    
    def __lt__(self, other):
        """
        Check if objects are less that each other using their names.

        Parameters
        ----------
        other : Crater
            Object to compare to.

        Returns
        -------
        bool
            True if name of this object is less than name of other.

        """
        return self.name < other.name
        

    def add_source(self, source):
        """
        Add a source to this crater object, checks if souces already has been added.

        Parameters
        ----------
        source : Crater_source
            The source to add.

        Returns
        -------
        None.

        """
        if (source not in self):
            self.sources.append(source)
        
        
    def remove_source(self, pos=None):
        """
        Remove source at position. If no pos is given the first source will be popped.

        Parameters
        ----------
        pos : int, optional
            The position of the source to remove. The default is None.

        Returns
        -------
        Crater_source
            The removed crater source.

        """
        return self.sources.pop(pos)           

    @staticmethod
    def from_row(row, refrence=Refrence(), f=defualt_from_csv):
        """
        Create a crater object from a row element.
        
        Parameters
        ----------
        row : list
            The list containg data
        refrence : Refrence, optional
            The refrence to use when creating a new crater object. The default is Refrence().
        f : function, optional
            The function to use to convert the row to correct variables. The default is defualt_from_csv.

        Returns
        -------
        crater : Crater
            The new crater with the populated information

        """
        name, lon, lat, d = f(row)
        c_source = Crater_source(lon, lat, d, refrence)
        crater = Crater(name)
        crater.add_source(c_source)
        return crater
        

        
def add_to_craters(craters, crater):
    """
    Check if crater already exsists in dictonary and adds crater acordingly.

    Parameters
    ----------
    craters : dictonary
        The dictonary to add crater to
    crater : Crater
        The crater to add

    Returns
    -------
    None.

    """
    #Check if crater already exist in craters
    if (crater.name in craters):
        #Add sources from crater to the crater already in craters 
        for s in crater.sources:
            craters[crater.name].add_source(s)
    #Crater does not already exist, add new entry 
    else:
        craters[crater.name] = crater
        

def add_from_csv(craters, path, refrence=Refrence(), func=defualt_from_csv, skip_header=True):
    """
    Add craters from csv file to list of craters, using the refrence and function provided.

    Parameters
    ----------
    craters : list
        The list to add the new craters to
    path : string
        The path to the csv file
    refrence : Refrence, optional
        The refrence to use when creating new craters. The default is Refrence().
    func : function, optional
        The function to use to convert csv data to crater data. The default is defualt_from_csv.
    skip_header : bool, optional
        Wheter or not to skip the first line in the csv. The default is True.

    Returns
    -------
    None.

    """
    with open(path) as f:
        r = csv.reader(f)
        if(skip_header):
            next(r)
        for row in r:
            temp_crater = Crater.from_row(row, refrence, func)
            add_to_craters(craters, temp_crater)
           
def print_craters(craters, key=None):
    """
    Iterate through sorted craters and prints each. If no key is given, dict will be sorted by names using __lt__.

    Parameters
    ----------
    craters : dict
        A dictonary of the craters.
    key : function, optional
        The function to use for sorting. The default is None.

    Returns
    -------
    None.

    """
    for c in sorted(craters.values(), key=key):
        print(c)
       

def clean_crater(crater):
    """
    Fill in defualt values for crater sources with missing values.
    
    Parameters
    ----------
    crater : Crater.Crater
        The crater which sources to clean.

    Returns
    -------
    None.

    """
    for source in crater.sources:
        if (source.diameter == None):
            source.diameter = 0.0
        if (source.lat == None):
            source.lat = 0.0
        if (source.lon == None):
            source.lon = 0.0
        
def combine_crater_sources(crater): 
    """
    Crate a new source entry for craters with multiple sources by taking the mean of their values.

    Parameters
    ----------
    crater : Crater.Crater
        The crater which sources to average.

    Returns
    -------
    None.

    """
    #Only need to combine if more than one source
    if (len(crater.sources) > 1):
        crater_data = []
        for source in crater.sources:
            crater_data.append([source.diameter, source.lat, source.lon])
        crater_data = np.array(crater_data, dtype=np.float64)
        #Calculate mean of diameter, lon and lat
        diameter, lat, lon = np.nanmean(crater_data, axis=0)
        combined_source = Crater_source(lon=lon, lat=lat, diameter=diameter, refrence=Refrence("Combined", 2021))
        crater.sources.insert(0, combined_source)#Add combinded source first in list
   
    #Fill in sources with missing values
    clean_crater(crater)

def get_craters():
    """
    Generate a dictonary of craters using a set of souce files.

    Returns
    -------
    craters : dict
        A dictonary of the craters.

    """
    craters = {}
    add_from_csv(craters, './crater_catalog/Neuman_2015.csv', Refrence("Neumann et.al", 2015, "10.1126/sciadv.1500852"))
    add_from_csv(craters, './crater_catalog/Oberst_2010.csv', Refrence("Oberst et.al", 2010))
    add_from_csv(craters, './crater_catalog/baker_2017.csv', Refrence("Baker et.al", 2017, "10.1016/j.icarus.2017.03.024"))
    add_from_csv(craters, "./crater_catalog/wood_2004.csv", Refrence("C.A. Wood", 2004))
    add_from_csv(craters, './crater_catalog/cook_2002.csv', Refrence("Cook et.al", 2002))
    #Average values for craters with multiple sources
    for crater in craters.values():
        combine_crater_sources(crater)
    return craters
    
if __name__ == "__main__":
    craters = get_craters()
    print_craters(craters, key=lambda item: item.sources[0].diameter)
