"""This package holds two classes for dealing with pyshtools and analysing its data for lunar craters."""
__version__ = '1.0' 
__author__ = 'Sindre Aalhus'
__date__ = "2021-04-01"
