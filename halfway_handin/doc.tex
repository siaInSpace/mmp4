\documentclass[a4paper,10pt]{article}

%%%%%%%%%% Variable setup %%%%%%%%%%
\def \authorName {Sindre Aalhus}
\def \studentRefNumber {170072577}
\def \userId {sia20}
\def \email {\userId@aber.ac.uk}
\def \assignmentName {Rim methods performance review}
\def \moduleName {PHM5860}
\def \moduleRef {}
\def \institution {Aberystwyth University}
\def \dueDate{TODO}

%%%%%%%%%% Margin setup %%%%%%%%%%%
\usepackage[
a4paper,
headheight = 10pt,
margin = 100pt,
tmargin = 60pt,
left = 50pt,
right = 50pt
]{geometry}

%%%%%%%%%% Header and footer %%%%%%
\usepackage{fancyhdr}
\usepackage{lastpage}

%%%%%%%%%% figures / image %%%%%%%%
\usepackage{graphicx}
\graphicspath{./images}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{wrapfig}

%%%Font / text manipulation %%%%%%%
\usepackage[hidelinks]{hyperref}  
\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={red!50!black},
    citecolor={red!50!black},
    urlcolor={blue!80!black}
}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{amsmath} %math equations

%\usepackage{indentfirst}
%\setlength{\parindent}{2em}
%\setlength{\parskip}{1em}


%%%%%%%%%% Header & Footer setup %%
\setlength{\headheight}{24pt}
\addtolength{\footskip}{20pt}
\fancypagestyle{plain}{
    \fancyhf{}
    \fancyhead[L]{{\userId \\ \studentRefNumber}}
    \fancyhead[R]{{\moduleRef\ \moduleName \\ \assignmentName}}
    \fancyfoot[L]{{\institution}}
    \fancyfoot[R]{{Page\ \thepage\ of\ \pageref{LastPage}}} 
    \renewcommand{\headrulewidth}{0.5pt}
    \renewcommand{\footrulewidth}{0.5pt}
}

\pagestyle{plain}
\author{\authorName (\studentRefNumber) \\ {\href{mailto:\email}\email}}        
%\date{Due on \dueDate}
\title{\assignmentName}

\usepackage{csquotes}
\usepackage[eprint=false, url=true, doi=true, sorting=none, urldate=comp]{biblatex}
\addbibresource{sources.bib}

\usepackage[noabbrev, nameinlink]{cleveref}
\usepackage{wrapfig} 
\usepackage{titlesec}
\usepackage{gensymb}

\begin{document}
\maketitle

\begin{abstract}
This document will serve as to review three methods to find the rim diameter of a known basins. In total each method has been compared against over 100 know 
basins ranging from just under 300km up to the south-pole atiken at $\sim$2500km using both topology data in a DEM and Bouguer anomaly data derived from the 
GRAIL 
mission. One of the methods was found to significantly outperform the two others and will be used in further development of a program to discover degraded 
basins.
\end{abstract}
\section{Data}
\begin{wrapfigure}[14]{r}{0.5\textwidth}
\centering
\includegraphics[width=0.48\textwidth]{images/Bailyy_y_max_pos_rotated_2020-02-17}
\caption{Results from using the Max method rotated across the Bailly basin. Note the symmetry either side of the 180 degrees as a result of sampling the same 
point}
\label{fig:max_bailly}
\end{wrapfigure}
The data for each basins was found by rotating a spherical harmonic model such that it was centered on the basin position, this ensures minimal deformation 
when expanding the spherical harmonic model into a 2d plane for image processing. A snippet of the data was then extracted by referencing the known diameter 
and 
selecting a square area around the center with side lengths equal to 2 times the known diameter in order to also copy any ring formations outside the rim 
diameter. However, it is important to note that with very large basins, such as the south-pole atiken, the area surrounding the basins becomes large enough to 
give some deformation when projecting 
to a 2d surface. The snippet was then resized to a 512x512 square for a better comparison between basins of different sizes. 

\section{Methods}
\begin{wrapfigure}[15]{r}{0.5\textwidth}
\vspace{-1.25cm}
\centering
\includegraphics[width=0.48\textwidth]{images/Bailly_Step_Fit_2020-02-10}
\caption{Data, initial fit and best fit from using the fit method across one line of the Bailly basins. Note the left peak has fitted well and the right side 
has fitted well to the rim edge without a clear peak.}
\label{fig:fit_bailly}
\end{wrapfigure}

Each method starts by extracting a horizontal line going through the center of the basin this reduces the data from two dimensions down to only one. This one 
dimensional data is then sent to the method in order to find the peaks. This is then repeated rotating the two dimensional data around its center and thus 
giving a new horizontal line across. This is done for a rotation of 0-360 degrees with a one degree increment. Finally the results from each rotation is 
averaged in order to find the crater center and rim diameter. 

\subsection{Max}
The first method developed finds the maximum value of the data and returns it. The maximum value should indicate the rim of the basin and thus when rotating 
around the entire basins can serve as a good indication of the rim location. However, this method only considers one point in each set and methods considering 
the entire data should perform better. From \Cref{fig:max_bailly} we can see the results of using this method rotated across the Bailly basin. The center is 
found by taking the mean of all points and the radius is taken as the standard deviation.

\subsection{Fit}

The second method attempts to do this by fitting a composite curve to the data. Specifically this method uses a step-up step-down function combined with a 
step-up function all using a arctan step to provide some smoothness to edges and peaks. This method considers the entire data and attempts to fit the curve and 
shows somewhat better results than the Max method. A possible improvement to this method could be to implement another function to model the background shape 
of the data and leave the step functions to deal with the peaks. However, this proved to be quite difficult as the basins offer a large amount of variation in 
data shape, especially when considering multi-ring vs single-ring basins. After finding the best fit this method returns the left and right edge of the step 
functions as the left and right rim as the basin. \Cref{fig:fit_bailly} shows this method using an initial guess and the result of fitting this function to the 
data, as seen in the red line. The left peak has fitted well by the function and even with the lack of a well defined peak on the right side the function has 
fitted well to the rim edge.

\subsection{Peak}
The third and final method evaluated is similar to the Max method. However instead of just taking the maximum value it evaluates possible positions as peaks 
by considering their prominence (how distinctive it is from surrounding data) and distance to other peaks, other attributes can also be considered such as 
vertical distance to neighbour and plateau size. This method showed the best results over the entire 
basin catalog. However, in some instances this method could not detect the basin, this can be a result of the parameters supplied and attributes considered. 
Further exploration on these parameters can result in even better performance. However it is also possible that some trade off must be made in order to detect 
all peaks, which can again reduce performance.

As this method does not have a clear range for how many peaks are returned some filtering was necessary to ensure only two peaks was passed on, representing 
the left and right side of the basin rim.   
This was done by finding the position of the leftmost and rightmost peak, this position is then updated for each subsequent peak averaging the position 
depending one which side is closest until only two peaks are left. This is then returned as the left and right side of the basin rim and later used to 
calculate the basin center and diameter. One could possibly improve this method by considering the prominence of each peak returned as an indication of how 
likely it is that the peak is the rim edge. 

\section{Results}
\input{figures}
\newpage
From \Cref{fig:d_err} we can see how the three different methods compare, \Cref{fig:d_abs_err} shows the absolute error value and can be more useful for 
numerical comparison together with \Cref{tbl:d_abs_err}. Firstly we can confirm that the peak method gives the best results both when using topology and 
gravitational data. Secondly we can see higher performance when using gravitational data over topology, most notably in when looking at spread of the upper and 
lower quartile with some improvement in mean and standard deviation. However, from \Cref{tbl:d_err} we can see that the Peak method only returned 92 (90) out 
of the 105 basins, this is likely a result of the parameters used to determine peaks from this method. While this is a decent (86\%) rate it could be improved 
by further study of the parameters and inner workings of the method used. 
\input{tables}

\section{Conclusion and way forward}
As we can see from the results using the peak method on gravitational data has proven to give the best results when attempting to find the rim diameter on a 
known basin location. Both the Fit and Max method showed promising results, Max with a smaller spread and Fit with a better mean. However, both were 
outperformed by the peak method. Other methods can be considered to locate the rim of a basin; one could look at the derivative of the data in order, this 
might give good results, but it would be important to apply some smoothing to the data as derivatives often struggle with noisy data. Use of edge detectors 
could and have been considered in combination with a feature detector such as hough-circles, during initial testing this showed promising results, but was 
abandoned for a simpler approach using the methods described. 

The way forward will be an attempt at improving the peak method in order to detect all basins and possibly improve performance by tweaking the parameters used. 
Once this is completed an attempt will be made to scan the lunar surface using this method in order to discover degraded basins, or at least 
re-discover the location of already known basins. 
\end{document}

