#!/bin/bash

pydoc -w program
pydoc -w program.main
pydoc -w program.Crater_tools
pydoc -w program.Crater_tools.Crater
pydoc -w program.Crater_tools.pyshtools_read

pydoc -w program.test_scripts
pydoc -w program.test_scripts.plots
pydoc -w program.test_scripts.custom_2d_fit
pydoc -w program.test_scripts.fitted_peak3_test
pydoc -w program.test_scripts.imageRead
pydoc -w program.test_scripts.test_multiprecessing
pydoc -w program.test_scripts.test_multithreading
pydoc -w program.test_scripts.two_d_fit
